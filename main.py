#!/usr/bin/env python3
############################################################
# Train and test models.
############################################################
# To see which models and what other settings are available
# see: main.py --help
############################################################

import argparse
import traceback
import trainers as trainer_import

import datetime
import numpy as np
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'  # or any {'0', '1', '2'}

import random
import tensorflow as tf
# noinspection PyUnresolvedReferences
from trainers import (
    trainer_tensorflow,
    trainer_bigru_klearn,
    trainer_distilbert_klearn,
    trainer_gru_klearn,
    trainer_fasttext_klearn,
    trainer_tuner
)
import mlflow
import mlflow.tensorflow
import logging
import shutil


ERROR_FILE_NAME="error.log"
if os.path.isfile(ERROR_FILE_NAME):
    os.remove(ERROR_FILE_NAME)

def parseArgs():
    """
    Parse command-line arguments.

    Returns:
        Run configuration.
    """

    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument(
        "--trainer",
        type=str,
        default="basic_tf",
        help=f"Trainer: basic_tf, bi_tf, bigru_klearn, distilbert_klearn, gru_klearn, gru_tf, grure_tf, fasttext_klearn, fasttext_tf",
    )

    parser.add_argument(
        "--data",
        type=str,
        default="",
        help="""Choose one of the following preprocessed data methods
        if you use an empty string e.g. --data "" then
        the current files in ./datasets/train.txt ./datasets/validate.txt ./datasets/test.txt will be used
    "whitelist_blacklist"
    "whitelist_blacklist_unbalanced"
    "whitelist_blacklist_cutted"
    "whitelist_blacklist_and_dga"
    "whitelist_blacklist_and_dga_cutted"
    """,
    )
    parser.add_argument(
        "--epochs",
        type=int,
        default=5,
        help="number of epochs",
    )
    parser.add_argument(
        "--learning_rate",
        type=float,
        default=1e-3,
        help="the learning rate",
    )
    parser.add_argument(
        "--max_features", type=int, default=10000, help="number of tokens, used on every trainer except distilbert_klearn"
    )
    parser.add_argument(
        "--embedding_dim", type=int, default=32, help="size of embeddings, used only for tensorflow trainers"
    )
    parser.add_argument("--batch_size", type=int, default=32, help="size of batch")
    parser.add_argument("--seed", type=int, default=42, help="The seed to be used")
    parser.add_argument("--reload", default=False, action="store_true", help="Used to reload a previous session, when using trainer_tuner")

    return parser.parse_args()

def main(config):
    """
    Starting point for the creation/training the models.

    For examples see the README file

    for help call:

    python main.py --help

    Args:

    Returns:

    """
    os.environ['PYTHONHASHSEED']=str(config.seed)
    os.environ['TF_CUDNN_DETERMINISTIC'] = '1'
    random.seed(config.seed)
    np.random.seed(config.seed)
    tf.random.set_seed(config.seed)
    # generate Trainer

    tf.get_logger().setLevel(logging.ERROR)
    if config.trainer.lower() == "tuner" and config.model is None:
        print("Tuner is only able to use with --model parameter")
        exit()
    prep = getattr(trainer_import, "trainer_" + "tensorflow")
    prepare_fct = getattr(prep, "Trainer" + "Tensorflow")

    trainer_class_name = config.trainer.lower()[0].capitalize() + config.trainer.lower()[1:]
    if trainer_class_name.lower() in ["gru_klearn", "fasttext_klearn", "distilbert_klearn", "bigru_klearn", "tuner"]:
        prep = getattr(trainer_import, "trainer_" + trainer_class_name.lower())
        prepare_fct = getattr(prep, "Trainer" + trainer_class_name)

    trainer = prepare_fct(**config.__dict__)

    if config.reload:
        trainer.train(**config.__dict__)
        return

    if config.trainer.lower() == "tuner":
        trainer_class_name = config.trainer.lower() + "_" + config.model.lower() + "_model"

    if config.data:
        print(f"Data: ./datasets/data-prepared-cleaned/{config.data}")
        if not os.path.exists(f"./datasets/data-prepared-cleaned/{config.data}"):
            print(f"The directory ./datasets/data-prepared-cleaned/{config.data} does not exist")
            print("Information about the acceptable options can be found at python main.py --help ")
            print("Please have a look into the README.md file at the chapter Installation -> Data")
            exit()

        def remove_file_if_exists(path):
            """Remove file if exists"""
            if os.path.isfile(path) or os.path.islink(path):
                os.remove(path)
            return

        remove_file_if_exists("./datasets/train.txt")
        remove_file_if_exists("./datasets/validate.txt")
        remove_file_if_exists("./datasets/test.txt")

        shutil.copy(f"./datasets/data-prepared-cleaned/{config.data}/test.txt", "./datasets/" )
        shutil.copy(f"./datasets/data-prepared-cleaned/{config.data}/train.txt", "./datasets/" )
        shutil.copy(f"./datasets/data-prepared-cleaned/{config.data}/validate.txt", "./datasets/")

    mlflow.tensorflow.autolog(silent=True)
    mlflow.set_experiment(trainer_class_name)
    trainer_class_name = trainer_class_name + "-" + datetime.datetime.now().strftime("%H_%M_%S-%d_%m_%Y")
    # run training

    with mlflow.start_run(run_name=trainer_class_name):
        mlflow.log_param("data", config.data)
        mlflow.log_param("seed_param", config.seed)
        mlflow.log_param("batch_size_param", config.batch_size)
        mlflow.log_param("learning_rate_param", config.learning_rate)
        mlflow.log_param("max_features_param", config.max_features)
        try:
            trainer.train(**config.__dict__)

        except Exception as e:
            traceback.print_exc()
            f = open(ERROR_FILE_NAME, "a")
            f.write(str(e))
            f.write(''.join(traceback.format_exception(None, e, e.__traceback__)))
            f.close()
            mlflow.log_artifact(ERROR_FILE_NAME)
            exit("Error occurred")


if __name__ == "__main__":
    tracking_url = os.environ.get('MLFLOW_TRACKING_URI')
    if tracking_url is not None:
        mlflow.set_tracking_uri(tracking_url)
    main(parseArgs())
