############################################################
# Collection of type functions to extend pythons
# argument parser library argparse.
############################################################

from argparse import Action, ArgumentError, ArgumentTypeError
from os import path


def intOrStr(v):
    """
    Convert command-line argument value to integer if possible.

    Args:
        v: command-line argument value

    Returns:
        value as integer if possible;
        otherwise value as string
    """
    if v.isdigit():
        return int(v)
    return v


def str2bool(v):
    """
    Convert command-line argument value to boolean.

    Args:
        v: command-line argument value

    Returns:
        value as boolean

    Source:
        https://stackoverflow.com/a/43357954
    """
    if isinstance(v, bool):
        return v
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    raise ArgumentTypeError('Boolean value expected.')


def fromEnum(T):
    """
    Convert command-line argument value to specific Enum.

    Args:
        T: Enum type

    Returns:
        type function which converts command-line value to Enum T
    """
    def test(v):
        """
        Convert command-line argument value to Enum T.

        Args:
            v: command-line argument value

        Returns:
            value as Enum T instance
        """
        # look for enum key (case insensitive)
        for e in T:
            if str(v).upper() == e.name.upper():
                return e

        # look for enum value
        for e in T:
            try:
                v2 = type(e.value)(v)
                if v2 == e.value:
                    return e
            except Exception:
                pass

        # neither enum key not value found
        raise ArgumentError()

    return test


def file(v):
    """
    Check if command-line argument value is existing file path.

    Args:
        v: command-line argument value

    Returns:
        file path with expanded home directory if used
    """
    f = path.expanduser(v)
    if path.isfile(f):
        return f
    raise ArgumentTypeError('Existing file expected.')


def directory(exist=True, expanduser=True, expandvars=True, abspath=False):
    """
    Check if directory path is valid.

    Args:
        exists:     directory path needs to exists (Default: True)
        expanduser: use absulute home path instead of tilde (Default: True)
        expandvars: use absulute path instead of path variable (Default: True)
        abspath:    use absulute path instead of relative path (Default: False)

    Returns:
        type function which evaluates command-line value
    """
    def test(v):
        """
        Check and adjust directory path.

        Args:
            v: command-line argument value

        Returns:
            adjusted and valid directory path
        """
        d = v
        if expanduser:
            d = path.expanduser(d)
        if expandvars:
            d = path.expandvars(d)
        if abspath:
            d = path.abspath(d)

        if exist:
            if path.isdir(d):
                return d
        else:
            if not path.exists(d) or path.isdir(d):
                return d

        raise ArgumentTypeError('Existing directory expected')

    return test


class StoreDictKeyPair(Action):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def __call__(self, parser, namespace, values, option_string):
        dest = {}
        for key_value_pair in values:
            key, value = key_value_pair.split('=')

            if value.isdigit():
                value = int(value)
            elif value.replace('.','',1).isdigit():
                value = float(value)

            dest[key] = value
        setattr(namespace, self.dest, dest)
