#!/usr/bin/env python3

import json
import matplotlib.pyplot as plt
import os
import numpy as np
from sklearn.metrics import classification_report


class Saver:
    def save_history_plot(history, modelname, epochs) -> str:
        acc = 0
        val_acc = 0

        loss = 0
        val_loss = 0

        if "accuracy" in history.history:
            acc = history.history["accuracy"]
            val_acc = history.history["val_accuracy"]

            loss = history.history["loss"]
            val_loss = history.history["val_loss"]

        else:
            acc = history.history["binary_accuracy"]
            val_acc = history.history["val_binary_accuracy"]

            loss = history.history["loss"]
            val_loss = history.history["val_loss"]

        epochs_range = range(len(acc))
        plt.figure(figsize=(12, 8))
        plt.subplot(1, 2, 1)
        plt.plot(epochs_range, acc, label="Training Accuracy")
        plt.plot(epochs_range, val_acc, label="Validation Accuracy")
        plt.legend(loc="lower right")
        plt.xlabel("epoch")
        plt.ylabel("accuracy")
        # plt.ylim(None, 1)
        plt.title("Training and Validation Accuracy")

        plt.subplot(1, 2, 2)
        plt.plot(epochs_range, loss, label="Training Loss")
        plt.plot(epochs_range, val_loss, label="Validation Loss")
        plt.xlabel("epoch")
        plt.ylabel("loss")
        plt.legend(loc="upper right")
        # plt.ylim(0, None)
        plt.title("Training and Validation Loss")

        if not os.path.isdir("plots"):
            os.mkdir("plots")
        figurepath = os.path.join("plots", modelname + "-history.png")
        plt.savefig(figurepath)

        return figurepath

    def save_history_json(history, modelname) -> str:
        if not os.path.isdir("history"):
            os.mkdir("history")
        class NumpyFloatValuesEncoder(json.JSONEncoder):
            def default(self, obj):
                if isinstance(obj, np.float32):
                    return float(obj)
                return JSONEncoder.default(self, obj)
        save_history_json_path = os.path.join("history", modelname + "-history.json")
        with open(save_history_json_path, "w") as file:
            json.dump(history.history, file, cls=NumpyFloatValuesEncoder)
        return save_history_json_path

    def save_model_summary(model, modelname) -> str:
        if not os.path.isdir("history"):
            os.mkdir("history")
        save_path = os.path.join("history", modelname + "-summary.txt")
        with open(save_path, "w") as f:
            model.summary(print_fn=lambda x: f.write(x + "\n"))
        return save_path

    def save_classification_report_json(truth_numpy, prediction_numpy, modelname):
        if not os.path.isdir("eval"):
            os.mkdir("eval")

        report_dict = classification_report(
            truth_numpy, prediction_numpy, target_names=["neg", "pos"], output_dict=True
        )

        save_classification_report_json_path = os.path.join(
            "eval", modelname + "-eval.json"
        )
        with open(save_classification_report_json_path, "w") as f:
            json.dump(report_dict, f)
        return save_classification_report_json_path, report_dict

    def save_classification_report(truth_numpy, prediction_numpy, modelname, class_names=["neg", "pos"]):
        if not os.path.isdir("eval"):
            os.mkdir("eval")

        report = classification_report(
            truth_numpy, prediction_numpy, target_names=class_names
        )

        save_path_evaluation = os.path.join("eval", modelname + "-eval.txt")
        if not os.path.isdir("eval"):
            os.mkdir("eval")

        with open(save_path_evaluation, "w") as f:
            f.write(str(report))
        return save_path_evaluation, report
