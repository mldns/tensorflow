#!/usr/bin/env python3
############################################################
# Inference, test trained models
############################################################
# To see which models and what other settings are available
# see: inference.py --help
############################################################

import os
import shutil
import argparse, sys
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'  # or any {'0', '1', '2'}
import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np
from sklearn.metrics import classification_report, confusion_matrix
import tensorflow_datasets as tfds
import tensorflow as tf

def main():
    """
    Tests the given tensorflow model

    Creates plots and shows evaluation of tensorflow models

    for help call:

    python main.py --help

    Args:

    Returns:

    """
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--model",
        type=str,
        default="bi_model",
        help="Name of directory in trained_models/: basic_tf_model, bi_tf_model, gru_tf_model, grure_tf_model, fasttext_tf_model",
    )

    parser.add_argument(
        "--data",
        type=str,
        default="whitelist_blacklist",
        help="""Choose one of the following preprocessed data methods
    "whitelist_blacklist"
    "whitelist_blacklist_unbalanced"
    "whitelist_blacklist_cutted"
    "whitelist_blacklist_and_dga"
    "whitelist_blacklist_and_dga_cutted"
    """,
    )
    parser.add_argument("--path", type=str, default=None, help="Path to directory of model")

    config = parser.parse_args()

    if config.data:
        if not os.path.exists(f"./datasets/data-prepared-cleaned/{config.data}"):
            print(f"The directory ./datasets/data-prepared-cleaned/{config.data} does not exist")
            print("Information about the acceptable options be found at python main.py --help ")
            print("Please have a look into the README.md file at the chapter Installation -> Data")
            exit()


        def remove_file_if_exists(path):
            """Remove file if exists"""
            if os.path.isfile(path) or os.path.islink(path):
                os.remove(path)
            return
        remove_file_if_exists("./datasets/train.txt")
        remove_file_if_exists("./datasets/validate.txt")
        remove_file_if_exists("./datasets/test.txt")
        shutil.copy(f"./datasets/data-prepared-cleaned/{config.data}/test.txt", "./datasets/" )
        shutil.copy(f"./datasets/data-prepared-cleaned/{config.data}/train.txt", "./datasets/" )
        shutil.copy(f"./datasets/data-prepared-cleaned/{config.data}/validate.txt", "./datasets/")

    path = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    path = os.path.join(path, "trained_models", config.model)

    if config.path is not None:
        path = config.path
    model = tf.keras.models.load_model(path)  # Folder

    from datasets.dataset import get_dataset

    is_imbalanced = "imbalanced" in config.model
    _, _, test_set = get_dataset(imbalanced=is_imbalanced)
    model.summary()
    print("-------CLASSIFICATION REPORT--------")
    test_text = test_set.map(lambda x, y: x)
    test_labels = test_set.map(lambda x, y: y)

    prediction = model.predict(test_text)
    prediction = tf.greater(prediction, 0.5)
    prediction_numpy = prediction.numpy().flatten().astype(int)

    truth_numpy = list(tfds.as_numpy(test_labels))
    truth_numpy = np.concatenate(truth_numpy).flatten()

    report = classification_report(
        truth_numpy, prediction_numpy, target_names=["neg", "pos"]
    )

    report_dict = classification_report(
        truth_numpy, prediction_numpy, target_names=["neg", "pos"], output_dict=True
    )
    print(report_dict)
    print(report)
    print("------")
    # print(confusion_matrix(truth_numpy, prediction_numpy))

    confusion_matrix(truth_numpy, prediction_numpy)
    tn, fp, fn, tp = confusion_matrix(truth_numpy, prediction_numpy).ravel()
    cf_matrix = np.array([[tp, fp], [fn, tn]])

    group_names = ["True Pos", "False Pos", "False Neg", "True Neg"]
    group_counts = ["{0:0.0f}".format(value) for value in cf_matrix.flatten()]
    group_percentages = [
        "{0:.2%}".format(value) for value in cf_matrix.flatten() / np.sum(cf_matrix)
    ]
    labels = [
        f"{v1}\n{v2}\n{v3}"
        for v1, v2, v3 in zip(group_names, group_counts, group_percentages)
    ]
    labels = np.asarray(labels).reshape(2, 2)
    print(labels)
    stats_text = "\n\nAccuracy={:0.3f}\n".format(report_dict["accuracy"])
    categories = ["White", "Black"]
    sns.heatmap(
        cf_matrix,
        xticklabels=categories,
        yticklabels=categories,
        annot=labels,
        fmt="",
        cmap="Blues",
    )
    plt.title(
        "Confusion Matrix, Test dataset with "
        + str(int(np.sum(cf_matrix)))
        + " samples \n"
        + config.model
    )
    plt.xlabel("Predictions" + stats_text)
    plt.ylabel("Actual")
    plt.draw()
    plt.pause(0.001)
    input("Press enter to continue...")
    plt.close()

if __name__ == "__main__":
    sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
    main()
