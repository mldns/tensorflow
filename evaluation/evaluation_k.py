#!/usr/bin/env python3
############################################################
# Inference, test trained models
############################################################
# To see which models and what other settings are available
# see: inference.py --help
############################################################

import os
import shutil
import argparse, sys
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'  # or any {'0', '1', '2'}
import tensorflow as tf
import ktrain
import numpy as np
from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report
import seaborn as sns
import matplotlib.pyplot as plt


def main():
    """
    Tests the given tensorflow model

    Creates plots and shows evaluation of tensorflow models

    for help call:

    python main.py --help

    Args:

    Returns:

    """
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--model",
        type=str,
        default="gru_model",
        help="bigru_klearn_model, distilbert_klearn_model, fasttext_klearn_model, gru_klearn_model",
    )

    parser.add_argument(
        "--data",
        type=str,
        default="whitelist_blacklist",
        help="""Choose one of the following preprocessed data methods
    "whitelist_blacklist"
    "whitelist_blacklist_unbalanced"
    "whitelist_blacklist_cutted"
    "whitelist_blacklist_and_dga"
    "whitelist_blacklist_and_dga_cutted"
    """,
    )
    parser.add_argument("--path", type=str, default=None, help="Path to directory of model")
    config = parser.parse_args()


    if config.data:
        if not os.path.exists(f"./datasets/data-prepared-cleaned/{config.data}"):
            print(f"The directory ./datasets/data-prepared-cleaned/{config.data} does not exist")
            print("Information about the acceptable options be found at python main.py --help ")
            print("Please have a look into the README.md file at the chapter Installation -> Data")
            exit()

        def remove_file_if_exists(path):
            """Remove file if exists"""
            if os.path.isfile(path) or os.path.islink(path):
                os.remove(path)
            return
        remove_file_if_exists("./datasets/train.txt")
        remove_file_if_exists("./datasets/validate.txt")
        remove_file_if_exists("./datasets/test.txt")
        shutil.copy(f"./datasets/data-prepared-cleaned/{config.data}/test.txt", "./datasets/" )
        shutil.copy(f"./datasets/data-prepared-cleaned/{config.data}/train.txt", "./datasets/" )
        shutil.copy(f"./datasets/data-prepared-cleaned/{config.data}/validate.txt", "./datasets/")

    from datasets.k_dataset import get_dataset

    is_imbalanced = "imbalanced" in config.model
    _, _, test_df = get_dataset(imbalanced=is_imbalanced)

    path = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    path = os.path.join(path, "trained_models", config.model)

    if config.path is not None:
        path = config.path

    predictor = ktrain.load_predictor(path)

    y_pred = predictor.predict(test_df["text"].to_list())
    y_true = test_df["pos"].to_list()
    y_pred = list(map(lambda x: 1 if x == "pos" or x == "1" else 0, y_pred))
    y_pred = np.squeeze(y_pred)
    y_true = np.squeeze(y_true)
    report = classification_report(y_true, y_pred, target_names=["neg", "pos"])
    report_dict = classification_report(y_true, y_pred, target_names=["neg", "pos"], output_dict=True)
    print(report)
    # print(confusion_matrix(y_true, y_pred))


    tn, fp, fn, tp = confusion_matrix(y_true, y_pred).ravel()
    cf_matrix = np.array([[tp, fp], [fn, tn]])

    group_names = ["True Pos", "False Pos", "False Neg", "True Neg"]
    group_counts = ["{0:0.0f}".format(value) for value in cf_matrix.flatten()]
    group_percentages = [
        "{0:.2%}".format(value) for value in cf_matrix.flatten() / np.sum(cf_matrix)
    ]
    labels = [
        f"{v1}\n{v2}\n{v3}"
        for v1, v2, v3 in zip(group_names, group_counts, group_percentages)
    ]
    labels = np.asarray(labels).reshape(2, 2)
    print(labels)
    stats_text = "\n\nAccuracy={:0.3f}\n".format(report_dict["accuracy"])
    categories = ["White", "Black"]
    sns.heatmap(
        cf_matrix,
        xticklabels=categories,
        yticklabels=categories,
        annot=labels,
        fmt="",
        cmap="Blues",
    )
    plt.title(
        "Confusion Matrix, Test dataset with "
        + str(int(np.sum(cf_matrix)))
        + " samples \n"
        + config.model
    )
    plt.xlabel("Predictions" + stats_text)
    plt.ylabel("Actual")
    plt.draw()
    plt.pause(0.001)
    input("Press enter to continue...")
    plt.close()


if __name__ == "__main__":
    sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
    main()
