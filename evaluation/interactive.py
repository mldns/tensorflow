#!/usr/bin/env python3
############################################################
# Inference, test trained models
############################################################
# To see which models and what other settings are available
# see: inference.py --help
############################################################

import argparse, os, sys
import numpy as np
import re, string

os.environ["TF_CPP_MIN_LOG_LEVEL"] = "3"  # or any {'0', '1', '2'}
import tensorflow as tf
import ktrain


def main():
    """
    Test domains interactive with tensorflow models

    for help call:

    python main.py --help

    Args:

    Returns:

    """
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--path", type=str, default=None, help="Path to trained model directory"
    )
    config = parser.parse_args()

    if config.path is not None:
        path = config.path

    model = None
    is_klearn = "klearn" in config.path
    if is_klearn:
        model = ktrain.load_predictor(path)
    else:
        model = tf.keras.models.load_model(path, compile=False)

    def custom_standardization(input_data):
        """


        Args:
          input_data:

        Returns:

        """
        lowercase = tf.strings.lower(input_data)
        text = tf.strings.regex_replace(lowercase, "-", " ")
        text = tf.strings.regex_replace(text, "_", " ")
        text = tf.strings.regex_replace(text, r"\.", " ")
        return tf.strings.regex_replace(
            text, "[%s]" % re.escape(string.punctuation), ""
        )

    while True:
        domain = input("\033[32m" + "Check domain (Press Enter to leave): " + "\033[0m")
        if not domain:
            print("\033[32m" + "Bye" + "\033[0m")
            exit()

        if not is_klearn:
            domain_preprocessed = custom_standardization(domain)
            domain_preprocessed = tf.reshape(domain_preprocessed, (1,))
            prediction = model.predict(domain_preprocessed)
            prediciton_text = "Benign" if tf.greater(prediction, 0.5) else "Malicious"
            prediction_proba = (
                prediction[0][0]
                if tf.greater(prediction, 0.5)
                else 1 - prediction[0][0]
            )
            print(f"Predicted: {prediciton_text} domain with probability {prediction_proba}")
        else:
            prediction = model.predict(domain)
            prediction_proba = model.predict_proba(domain)[1]
            prediciton_text = "Benign" if "pos" in prediction else "Malicious"
            prediction_proba = (
                prediction_proba if prediction_proba > 0.5 else 1 - prediction_proba
            )
            print(f"Predicted: {prediciton_text} domain with probability {prediction_proba}")


if __name__ == "__main__":
    sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
    main()
