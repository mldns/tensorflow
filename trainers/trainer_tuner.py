#!/usr/bin/env python3

from models.basic_tf_model import BasicHyperModel
from datasets.dataset import get_dataset
import tensorflow as tf
import os
import pandas as pd
from tensorflow.keras import layers
from tensorflow.keras.layers.experimental.preprocessing import TextVectorization
from tensorflow.keras import losses
import numpy as np
import tensorflow_datasets as tfds
import mlflow
from sklearn.utils import class_weight
import tensorflow_text  # import to prevent error: KeyError: 'CaseFoldUTF8'
from utils.saver import Saver as saver
from functools import partial
import kerastuner as kt
import datetime
import pickle
import sys


class TrainerTuner():
    """Class to train the Basic Model"""

    def __init__(
        self,
        epochs=15,
        learning_rate=0.001,
        max_features=100000,
        embedding_dim=32,
        batch_size=32,
        imbalanced=False,
        seed=42,
        trainer="basic",
        model="basic",
        reload=False,
        **kwargs,
    ):
        self.epochs = epochs
        self.learning_rate = learning_rate
        self.max_features = max_features
        self.embedding_dim = embedding_dim
        self.batch_size = batch_size
        self.imbalanced = imbalanced
        self.modelname = trainer.lower() + "_" + model.lower() + "_model"
        self.model = model.lower()
        self.seed = seed
        self.reload = reload

    def train(
        self,
        **kwargs,
    ):
        """
        Function which trains the basic model

        Retrieves the datasets and model and trains and test the model

        Args:

        Returns:

        """

        # get dataset
        train_ds, val_ds, test_ds = get_dataset(
            batch_size=self.batch_size,
            imbalanced=self.imbalanced,
        )

        cls_weight = None
        if self.imbalanced:
            print("-------------(Warning) Imbalanced Data is used----------")
            dirname = os.path.dirname(os.path.dirname(__file__))
            train_path = os.path.join(dirname, "datasets", "imbalanced", "train.txt")
            df = pd.read_csv(train_path, header=None, names=["label", "text"])

            df["label"] = df["label"].apply(lambda x: 0 if x == "__label__bad" else 1)
            train_labels = np.array(df["label"].to_list())

            cls_weight = class_weight.compute_class_weight(
                "balanced",
                classes=np.unique(train_labels),
                y=train_labels,
            )
            print("The class weights are")
            cls_weight = {0: max(cls_weight), 1: min(cls_weight)}
            print(cls_weight)
            print("-----------------")

        print(self.modelname)
        print(self.model)
        print(self.max_features)

        vectorize_layer = TextVectorization(
            standardize="lower_and_strip_punctuation",
            max_tokens=self.max_features,
            output_mode="int",
        )

        train_text = train_ds.map(lambda x, y: x)
        vectorize_layer.adapt(train_text)

        # get model
        get_model_hp = __import__(f'models.{self.model}_model', globals(), locals(), ['get_model']).get_model_hp

        get_model = partial(get_model_hp, encoder=vectorize_layer)

        if not os.path.isdir("log-tuner"):
            os.mkdir("log-tuner")

        tuner_project_name = self.modelname + "-" + datetime.datetime.now().strftime("%H_%M_%S-%d_%m_%Y")
        if self.reload:
            log_tuners = [directory for directory in os.listdir("log-tuner") if not "tensorboard" in directory]
            if len(log_tuners) > 0:
                print("Please choose which tuner session")
                for i, session in enumerate(log_tuners):
                    print(i, session)
                session_index = int(input("number: "))
                tuner_project_name = log_tuners[session_index]
                print(tuner_project_name)

        tuner = kt.Hyperband(
            get_model,
            objective="val_binary_accuracy",
            max_epochs=100,
            factor=3,
            project_name=tuner_project_name,
            seed=self.seed,
            directory="log-tuner",
        )


        tuner.search(
            train_ds,
            validation_data=val_ds,
            epochs=self.epochs,
            callbacks=[
                tf.keras.callbacks.EarlyStopping("val_binary_accuracy",patience=3),
                tf.keras.callbacks.TensorBoard(os.path.join("log-tuner", f"{tuner_project_name}-tensorboard")),
            ],
        )

        if self.reload:
            tuner.reload()
            models = tuner.get_best_models(num_models=2)

            original_stdout = sys.stdout
            with open(f"search_space_summary.txt", "w") as f:
                sys.stdout = f
                print(tuner.search_space_summary())
                print(tuner.results_summary())
                sys.stdout = original_stdout

            print(tuner.search_space_summary())
            exit()

        original_stdout = sys.stdout
        if not os.path.isdir("tuner-summary"):
            os.mkdir("tuner-summary")

        with open(os.path.join("tuner-summary", tuner_project_name+ "_search_space_summary.txt"), "w") as f:
            sys.stdout = f
            print(tuner.search_space_summary())
            sys.stdout = original_stdout
        print(tuner.search_space_summary())
        mlflow.log_artifact(os.path.join("tuner-summary", tuner_project_name+ "_search_space_summary.txt"))

        original_stdout = sys.stdout
        with open(os.path.join("tuner-summary", tuner_project_name+ "_results_summary.txt"), "w") as f:
            sys.stdout = f
            print(tuner.results_summary())
            sys.stdout = original_stdout
        print(tuner.results_summary())
        mlflow.log_artifact(os.path.join("tuner-summary", tuner_project_name+ "_results_summary.txt"))

        model = tuner.get_best_models(num_models=1)[0]

        # model = tuner.get_best_models()[0]
        print("-----evaluate---------------")
        loss, accuracy = model.evaluate(test_ds)


        print(model.summary())

        print(self.modelname)

        model.add(layers.Activation("sigmoid"))

        print(model.summary())
        save_path = os.path.join("trained_models", self.modelname)
        model.save(save_path)
        mlflow.log_artifact(save_path)

        def size_of_dir_in_mb(directory):
            def convert_bytes_to_mb(value):
                return value/float(1<<20)

            import subprocess
            import re
            import traceback
            dir_size_in_bytes = 0

            try:
                dir_size_in_bytes = subprocess.check_output(f"du -sb {directory}" + " | awk '{print $1}'",  stderr=subprocess.STDOUT, shell=True)
            except Exception as e:
                traceback.print_exc()
                f = open("error.log", "a")
                f.write(str(e))
                f.write(''.join(traceback.format_exception(None, e, e.__traceback__)))
                f.close()
                mlflow.log_artifact("error.log")
                return 0

            dir_size_in_bytes = re.search(r'\d+',dir_size_in_bytes.decode("utf-8", errors="ignore")).group()
            dir_size_in_bytes = int(dir_size_in_bytes)
            return convert_bytes_to_mb(dir_size_in_bytes)
        mlflow.log_metric("model_size_test", size_of_dir_in_mb(save_path))


        model = tf.keras.models.load_model(save_path)
        model.summary()

        metrics = [
            tf.keras.metrics.TruePositives(name="tp"),
            tf.keras.metrics.FalsePositives(name="fp"),
            tf.keras.metrics.TrueNegatives(name="tn"),
            tf.keras.metrics.FalseNegatives(name="fn"),
            tf.keras.metrics.BinaryAccuracy(name="accuracy"),
            tf.keras.metrics.Precision(name="precision"),
            tf.keras.metrics.Recall(name="recall"),
            tf.keras.metrics.AUC(name="auc"),
            tf.keras.metrics.AUC(name="prc", curve="PR"),  # precision-recall curve
        ]

        model.compile(
            loss=losses.BinaryCrossentropy(from_logits=False),
            # optimizer="adam",
            metrics=metrics,
        )
        model.evaluate(test_ds)

        print("-------CLASSIFICATION REPORT--------")
        test_text = test_ds.map(lambda x, y: x)
        test_labels = test_ds.map(lambda x, y: y)

        prediction = model.predict(test_text)
        prediction = tf.greater(prediction, 0.5)
        prediction_numpy = prediction.numpy().flatten().astype(int)

        truth_numpy = list(tfds.as_numpy(test_labels))
        truth_numpy = np.concatenate(truth_numpy).flatten()

        save_path_evaluation, report = saver.save_classification_report(truth_numpy=truth_numpy, prediction_numpy=prediction_numpy, modelname=self.modelname)
        print(report)
        mlflow.log_artifact(save_path_evaluation)
        print(f"classification report saved to: {save_path_evaluation}")

        print("------------------------------")

        save_classification_report_json_path, report_dict = saver.save_classification_report_json(truth_numpy=truth_numpy, prediction_numpy= prediction_numpy, modelname=self.modelname)
        print(f"classification Json report saved to: {save_classification_report_json_path}")
        mlflow.log_artifact(save_classification_report_json_path)

        mlflow.log_metric("neg_precision_test", report_dict["neg"]["precision"])
        mlflow.log_metric("neg_recall_test", report_dict["neg"]["recall"])

        mlflow.log_metric("pos_precision_test", report_dict["pos"]["precision"])
        mlflow.log_metric("pos_recall_test", report_dict["pos"]["recall"])

        mlflow.log_metric("accuracy_test", report_dict["accuracy"])


        print("------------------------------")

        save_path = saver.save_model_summary(model=model, modelname=self.modelname)
        print(f"Save Model Summary: {save_path}")
        mlflow.log_artifact(save_path)
        return


if __name__ == "__main__":
    from os import path
    import sys

    sys.path.append(path.join(path.dirname(__file__), ".."))
