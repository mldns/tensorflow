#!/usr/bin/env python3

from datasets.dataset import get_dataset
import tensorflow as tf
import os
import pandas as pd
from tensorflow.keras import layers
from tensorflow.keras.layers.experimental.preprocessing import TextVectorization
from tensorflow.keras import losses
import numpy as np
import tensorflow_datasets as tfds
import mlflow
from sklearn.utils import class_weight
import tensorflow_text  # import to prevent error: KeyError: 'CaseFoldUTF8'
from utils.saver import Saver as saver


class TrainerTensorflow():
    """Class to train the Basic Model"""

    def __init__(
        self,
        epochs=15,
        learning_rate=0.001,
        max_features=100000,
        embedding_dim=32,
        batch_size=32,
        imbalanced=False,
        seed=42,
        trainer="basic",
        **kwargs,
    ):
        self.epochs = epochs
        self.learning_rate = learning_rate
        self.max_features = max_features
        self.embedding_dim = embedding_dim
        self.batch_size = batch_size
        self.imbalanced = imbalanced
        self.modelname = trainer.lower() + "_model" if not imbalanced else trainer.lower() + "_model" + "-imbalanced"
        self.model = trainer.lower()
        self.seed = seed

    def train(
        self,
        **kwargs,
    ):
        """
        Function which trains the basic model

        Retrieves the datasets and model and trains and test the model

        Args:

        Returns:

        """

        # get dataset
        train_ds, val_ds, test_ds = get_dataset(
            batch_size=self.batch_size,
            imbalanced=self.imbalanced,
        )

        cls_weight = None
        if self.imbalanced:
            print("-------------(Warning) Imbalanced Data is used----------")
            dirname = os.path.dirname(os.path.dirname(__file__))
            train_path = os.path.join(dirname, "datasets", "imbalanced", "train.txt")
            df = pd.read_csv(train_path, header=None, names=["label", "text"])

            df["label"] = df["label"].apply(lambda x: 0 if x == "__label__bad" else 1)
            train_labels = np.array(df["label"].to_list())

            cls_weight = class_weight.compute_class_weight(
                "balanced",
                classes=np.unique(train_labels),
                y=train_labels,
            )
            print("The class weights are")
            cls_weight = {0: max(cls_weight), 1: min(cls_weight)}
            print(cls_weight)
            print("-----------------")

        print(self.modelname)
        # print(self.max_features)

        vectorize_layer = TextVectorization(
            standardize="lower_and_strip_punctuation",
            max_tokens=self.max_features,
            output_mode="int",
        )

        train_text = train_ds.map(lambda x, y: x)
        vectorize_layer.adapt(train_text)

        # get model
        get_model = __import__(f'models.{self.model}_model', globals(), locals(), ['get_model']).get_model
        model = get_model(
            vectorize_layer, self.embedding_dim, self.learning_rate
        )

        callback = tf.keras.callbacks.EarlyStopping(monitor='loss', patience=3)
        # train model
        history = model.fit(
            train_ds,
            validation_data=val_ds,
            epochs=self.epochs,
            class_weight=cls_weight,
            callbacks=[callback]
        )

        print("-----evaluate---------------")
        loss, accuracy = model.evaluate(test_ds)


        print(model.summary())

        print(f"Modelname: {self.modelname}")

        model.add(layers.Activation("sigmoid"))

        print(model.summary())
        save_path = os.path.join("trained_models", self.modelname)
        model.save(save_path)
        mlflow.log_artifact(save_path)

        def size_of_dir_in_mb(directory):
            def convert_bytes_to_mb(value):
                return value/float(1<<20)

            import subprocess
            import re
            import traceback
            dir_size_in_bytes = 0

            try:
                dir_size_in_bytes = subprocess.check_output(f"du -sb {directory}" + " | awk '{print $1}'",  stderr=subprocess.STDOUT, shell=True)
            except Exception as e:
                traceback.print_exc()
                f = open("error.log", "a")
                f.write(str(e))
                f.write(''.join(traceback.format_exception(None, e, e.__traceback__)))
                f.close()
                mlflow.log_artifact("error.log")
                return 0

            dir_size_in_bytes = re.search(r'\d+',dir_size_in_bytes.decode("utf-8", errors="ignore")).group()
            dir_size_in_bytes = int(dir_size_in_bytes)
            return convert_bytes_to_mb(dir_size_in_bytes)
        mlflow.log_metric("model_size_test", size_of_dir_in_mb(save_path))

        model = tf.keras.models.load_model(save_path)
        model.summary()

        metrics = [
            tf.keras.metrics.TruePositives(name="tp"),
            tf.keras.metrics.FalsePositives(name="fp"),
            tf.keras.metrics.TrueNegatives(name="tn"),
            tf.keras.metrics.FalseNegatives(name="fn"),
            tf.keras.metrics.BinaryAccuracy(name="accuracy"),
            tf.keras.metrics.Precision(name="precision"),
            tf.keras.metrics.Recall(name="recall"),
            tf.keras.metrics.AUC(name="auc"),
            tf.keras.metrics.AUC(name="prc", curve="PR"),  # precision-recall curve
        ]

        model.compile(
            loss=losses.BinaryCrossentropy(from_logits=False),
            # optimizer="adam",
            metrics=metrics,
        )
        model.evaluate(test_ds)

        print("-------CLASSIFICATION REPORT--------")
        test_text = test_ds.map(lambda x, y: x)
        test_labels = test_ds.map(lambda x, y: y)

        prediction = model.predict(test_text)
        prediction = tf.greater(prediction, 0.5)
        prediction_numpy = prediction.numpy().flatten().astype(int)

        truth_numpy = list(tfds.as_numpy(test_labels))
        truth_numpy = np.concatenate(truth_numpy).flatten()

        save_path_evaluation, report = saver.save_classification_report(truth_numpy=truth_numpy, prediction_numpy=prediction_numpy, modelname=self.modelname)
        print(report)
        mlflow.log_artifact(save_path_evaluation)
        print(f"classification report saved to: {save_path_evaluation}")

        print("------------------------------")

        save_classification_report_json_path, report_dict = saver.save_classification_report_json(truth_numpy=truth_numpy, prediction_numpy= prediction_numpy, modelname=self.modelname)
        print(f"classification Json report saved to: {save_classification_report_json_path}")
        mlflow.log_artifact(save_classification_report_json_path)

        mlflow.log_metric("neg_precision_test", report_dict["neg"]["precision"])
        mlflow.log_metric("neg_recall_test", report_dict["neg"]["recall"])

        mlflow.log_metric("pos_precision_test", report_dict["pos"]["precision"])
        mlflow.log_metric("pos_recall_test", report_dict["pos"]["recall"])

        mlflow.log_metric("accuracy_test", report_dict["accuracy"])


        print("------------------------------")

        mlflow.log_artifact(saver.save_history_json(history=history, modelname=self.modelname))

        print(history.history)
        figurepath = saver.save_history_plot(history=history, modelname=self.modelname, epochs=self.epochs)
        mlflow.log_artifact(figurepath)

        print("--------------------------------------------")
        save_path = saver.save_model_summary(model=model, modelname=self.modelname)
        print(f"Save Model Summary: {save_path}")
        mlflow.log_artifact(save_path)
        return


if __name__ == "__main__":
    from os import path
    import sys

    sys.path.append(path.join(path.dirname(__file__), ".."))
