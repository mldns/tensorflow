#!/usr/bin/env python3

from datasets.k_dataset import get_dataset
import os
import ktrain
from ktrain import text
from sklearn.metrics import classification_report
import numpy as np
import matplotlib.pyplot as plt
import mlflow, json
from sklearn.utils import class_weight
import tensorflow_text  # import to prevent error: KeyError: 'CaseFoldUTF8'
import pandas as pd
from utils.saver import Saver as saver


class TrainerDistilbert_klearn:
    """Class to train the Distilbert Model"""

    def __init__(
        self,
        epochs=1,
        learning_rate=8e-5,
        max_features=100000,
        embedding_dim=32,
        batch_size=32,
        imbalanced=False,
        trainer="distilbert_klearn",
        **kwargs,
    ):
        self.epochs = epochs
        self.learning_rate = learning_rate
        self.max_features = max_features
        self.embedding_dim = embedding_dim
        self.batch_size = batch_size
        self.name = "distilbert-base-uncased"
        self.imbalanced = imbalanced
        self.modelname = os.path.basename(__file__).split(".")[0].partition("trainer_")[2] + "_model"
    def train(
        self,
        **kwargs,
    ):
        """
        Function which trains the distilbert model

        Retrieves the datasets and model and trains and test the model

        Args:

        Returns:

        """

        # get dataset

        print(self.modelname)

        cls_weight = None
        if self.imbalanced:
            print("-------------(Warning) Imbalanced Data is used----------")
            self.modelname = self.modelname + "-imbalanced"
            dirname = os.path.dirname(os.path.dirname(__file__))
            train_path = os.path.join(dirname, "datasets", "imbalanced", "train.txt")
            df = pd.read_csv(train_path, header=None, names=["label", "text"])

            df["label"] = df["label"].apply(lambda x: 0 if x == "__label__bad" else 1)
            train_labels = np.array(df["label"].to_list())

            cls_weight = class_weight.compute_class_weight(
                "balanced",
                classes=np.unique(train_labels),
                y=train_labels,
            )
            print("The class weights are")
            cls_weight = {"neg": max(cls_weight), "pos": min(cls_weight)}
            print(cls_weight)
            print("-----------------")

        train_df, val_df, test_df = get_dataset(self.imbalanced)
        train_text, train_label = train_df["text"].to_list(), train_df["pos"].to_list()
        val_text, val_label = val_df["text"].to_list(), val_df["pos"].to_list()
        test_text, test_label = test_df["text"].to_list(), test_df["pos"].to_list()


        maxlength = max(len(ele) for ele in train_df["text"].to_list())
        t = text.Transformer(self.name, maxlen=maxlength, class_names=["neg", "pos"])
        trn = t.preprocess_train(train_text, train_label)
        val = t.preprocess_test(val_text, val_label)
        model = t.get_classifier()
        learner = ktrain.get_learner(model, train_data=trn, val_data=val, batch_size=6)


        # https://github.com/amaiya/ktrain/blob/master/ktrain/core.py
        # history = learner.fit_onecycle(self.learning_rate, self.epochs, class_weight=cls_weight)
        history = learner.autofit(lr=self.learning_rate, epochs=self.epochs)

        predictor = ktrain.get_predictor(learner.model, preproc=t)
        save_path = os.path.join("trained_models", self.modelname)
        predictor.save(save_path)
        mlflow.log_artifact(save_path)

        def size_of_dir_in_mb(directory):
            def convert_bytes_to_mb(value):
                return value/float(1<<20)

            import subprocess
            import re
            import traceback
            dir_size_in_bytes = 0

            try:
                dir_size_in_bytes = subprocess.check_output(f"du -sb {directory}" + " | awk '{print $1}'",  stderr=subprocess.STDOUT, shell=True)
            except Exception as e:
                traceback.print_exc()
                f = open("error.log", "a")
                f.write(str(e))
                f.write(''.join(traceback.format_exception(None, e, e.__traceback__)))
                f.close()
                mlflow.log_artifact("error.log")
                return 0

            dir_size_in_bytes = re.search(r'\d+',dir_size_in_bytes.decode("utf-8", errors="ignore")).group()
            dir_size_in_bytes = int(dir_size_in_bytes)
            return convert_bytes_to_mb(dir_size_in_bytes)
        mlflow.log_metric("model_size_test", size_of_dir_in_mb(save_path))

        test = t.preprocess_test(test_text, test_label)

        class_names = [str(s) for s in t.get_classes()]
        learner.evaluate(test, class_names=class_names)
        y_pred = learner.predict(val_data=test)
        y_true = learner.ground_truth(val_data=test)
        y_pred = np.squeeze(y_pred)
        y_true = np.squeeze(y_true)

        if len(y_pred.shape) == 1:
            y_pred = np.where(y_pred > 0.5, 1, 0)
            y_true = np.where(y_true > 0.5, 1, 0)
        else:
            y_pred = np.argmax(y_pred, axis=1)
            y_true = np.argmax(y_true, axis=1)

        truth_numpy = y_true
        prediction_numpy = y_pred
        save_path_evaluation, report = saver.save_classification_report(truth_numpy=truth_numpy, prediction_numpy=prediction_numpy, modelname=self.modelname, class_names=class_names)
        print(report)
        mlflow.log_artifact(save_path_evaluation)
        print(f"classification report saved to: {save_path_evaluation}")

        print("------------------------------")

        save_classification_report_json_path, report_dict = saver.save_classification_report_json(truth_numpy=truth_numpy, prediction_numpy= prediction_numpy, modelname=self.modelname)
        print(f"classification Json report saved to: {save_classification_report_json_path}")
        mlflow.log_artifact(save_classification_report_json_path)

        mlflow.log_metric("neg_precision_test", report_dict["neg"]["precision"])
        mlflow.log_metric("neg_recall_test", report_dict["neg"]["recall"])

        mlflow.log_metric("pos_precision_test", report_dict["pos"]["precision"])
        mlflow.log_metric("pos_recall_test", report_dict["pos"]["recall"])

        mlflow.log_metric("accuracy_test", report_dict["accuracy"])


        print("------------------------------")

        mlflow.log_artifact(saver.save_history_json(history=history, modelname=self.modelname))

        print(history.history)
        figurepath = saver.save_history_plot(history=history, modelname=self.modelname, epochs=self.epochs)
        mlflow.log_artifact(figurepath)

        print("--------------------------------------------")
        save_path = saver.save_model_summary(model=model, modelname=self.modelname)
        print(f"Save Model Summary: {save_path}")
        mlflow.log_artifact(save_path)
        return


if __name__ == "__main__":
    from os import path
    import sys

    sys.path.append(path.join(path.dirname(__file__), ".."))
