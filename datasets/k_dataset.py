#!/usr/bin/env python3

import pandas as pd
import os, re, string
import tensorflow as tf


def preprocess(path):
    """
    Preprocess a CSV File with label, text for Ktrain models

    Args:
      path: Path of the CSV File

    Returns:
      Pandas Dataframe, complete domains as a list, and labels as a list

    """
    df = pd.read_csv(path, header=None, names=["label", "text"])

    df["text"] = df["text"].apply(lambda x: re.sub(r"[\.|\-|\_]", r" ", x))
    df["label"] = df["label"].apply(lambda x: "neg" if x == "__label__bad" else "pos")
    df.columns = ["label", "text"]
    df = pd.concat([df, df.label.astype("str").str.get_dummies()], axis=1, sort=False)
    df = df[["text", "neg", "pos"]]
    return df, df["text"].to_list(), df["pos"].to_list()


def get_dataset(imbalanced=False):
    """
    Get the training, validation and test datasets

    Args:
      imbalanced: (Default value = False)

    Returns:
      training, validation and test dataframe

    """
    dirname = os.path.dirname(__file__)
    # mldns/dataset/whitelist.txt
    if imbalanced:
        print("--Imbalanced Data in datasets/imbalanced/ will be used")
        dirname = os.path.join(dirname, "imbalanced")
    train_path = os.path.join(dirname, "train.txt")
    validation_path = os.path.join(dirname, "validate.txt")
    test_path = os.path.join(dirname, "test.txt")

    train_df, train_text, train_label = preprocess(train_path)
    valid_df, valid_text, valid_label = preprocess(validation_path)
    test_df, test_text, test_label = preprocess(test_path)

    return train_df, valid_df, test_df


if __name__ == "__main__":
    train_df, valid_df, test_df = get_dataset()
    print(train_df)
