#!/usr/bin/env python3

import os
import re
import string
import tensorflow as tf
import pandas as pd


def get_dataset(batch_size=32, imbalanced=False):
    """
    Transform the data given by train.txt, validate.txt and test.txt to tensorflow datasets.
    Adds according to the data given by the text files the label and preprocess each line by the
    custom_standardization function.

    Args:
      batch_size: Size of the batches of data (Default value = 32)
      imbalanced: Set to True if imbalanced data is used (Default value = False)

    Returns:
      training_set, validation_set, test_set: Tensorflow Datasets

    """
    dirname = os.path.dirname(__file__)

    train_path = os.path.join(dirname, "train.txt")
    validation_path = os.path.join(dirname, "validate.txt")
    test_path = os.path.join(dirname, "test.txt")
    if imbalanced:
        print("--Imbalanced Data in datasets/imbalanced/ will be used")
        train_path = os.path.join(dirname, "imbalanced", "train.txt")
        validation_path = os.path.join(dirname, "imbalanced", "validate.txt")
        test_path = os.path.join(dirname, "imbalanced", "test.txt")

    def custom_standardization(input_data):
        """
        Takes a string and converts it to lower case.
        Also it replaces "-","_" and "." to spaces.
        And deletes all characters in '!"#$%&\'()*+,-./:;<=>?@[\\]^_`{|}~' from the string.

        Args:
          input_data: string

        Returns:
          Preprocessed Tensorflow string

        """
        lowercase = tf.strings.lower(input_data)
        text = tf.strings.regex_replace(lowercase, "-", " ")
        text = tf.strings.regex_replace(text, "_", " ")
        text = tf.strings.regex_replace(text, r"\.", " ")
        return tf.strings.regex_replace(
            text, "[%s]" % re.escape(string.punctuation), ""
        )

    def labeler(sample):
        """
        Labels the given line of the csv files given that the bad domains are labeld with 'label__bad'
        For bad domains it gives the label 0 and for a good labeled domain 1.
        Also deletes the label__ string from the line, so only the domain is preprocessed through the custom_standardization function.

        Args:
          sample: a line from the given csv file

        Returns:
          Preprocessed domain and label

        """
        if tf.strings.regex_full_match(sample, ".*label__bad.*"):
            text = tf.strings.regex_replace(sample, "^.*,", "")
            text = custom_standardization(text)
            return text, tf.cast(0, tf.int64)

        text = tf.strings.regex_replace(sample, "^.*,", "")
        text = custom_standardization(text)
        return text, tf.cast(1, tf.int64)

    train_raw_ds = (
        tf.data.TextLineDataset(train_path)
        .map(
            labeler,
            num_parallel_calls=tf.data.experimental.AUTOTUNE,
        )
        # .shuffle(buffer_size=1000)
        .batch(batch_size)
        .cache()
        .prefetch(tf.data.experimental.AUTOTUNE)
    )
    valid_raw_ds = (
        tf.data.TextLineDataset(validation_path)
        .map(
            labeler,
            num_parallel_calls=tf.data.experimental.AUTOTUNE,
        )
        # .shuffle(buffer_size=1000)
        .batch(batch_size)
        .cache()
        .prefetch(tf.data.experimental.AUTOTUNE)
    )

    test_raw_ds = (
        tf.data.TextLineDataset(test_path)
        .map(
            labeler,
            num_parallel_calls=tf.data.experimental.AUTOTUNE,
        )
        # .shuffle(buffer_size=1000)
        .batch(batch_size)
        .cache()
        .prefetch(tf.data.experimental.AUTOTUNE)
    )
    return train_raw_ds, valid_raw_ds, test_raw_ds


if __name__ == "__main__":
    train_dataset, val_dataset, test_dataset = get_dataset()
    for text_batch, label_batch in train_dataset.take(1):
        for i in range(3):
            print("Review", text_batch.numpy()[i])
            print("Label", label_batch.numpy()[i])
