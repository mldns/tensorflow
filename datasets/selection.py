############################################################
# Data Selection.
############################################################
# Collection of methods to fetch different train, validate
# and test datasets.
############################################################

from os import environ

from os.path import exists
from sklearn.utils import shuffle

import pandas as pd
from tld import get_fld, get_tld


# Dataset labels
label = {
    'top': '__label__top',
    'bad': '__label__bad',
}


def get_seed():
    """
    Get initialize internal state.

    Returns
    -------
    int
        Initialize internal state value.
        `None` if initial internal state should be randomly set.
    """
    seed = environ.get('PYTHONHASHSEED')
    if not seed:
        return None
    return int(seed)


class DataSelection:
    """
    Collection of methods to fetch different train, validate and test datasets.
    """

    def _split_merge_shuffle(X_allow, X_deny):
        """
        split, merge and shuffle
        60% - train set,
        20% - validation set,  (always ballanced)
        20% - test set         (always ballanced)

        Arguments
        ---------
        X_allow : array, np.ndarray or ndarray-like
            List of allowed domains.
        X_deny : array, np.ndarray or ndarray-like
            List of denied domains.

        Returns
        -------
        6-Tuple of np.ndarray
            X_train, y_train, X_validate, y_validate, X_test, y_test
        """
        random_state = get_seed()

        # create datasets
        df_allow = pd.DataFrame({ 'domain': X_allow })
        df_deny = pd.DataFrame({ 'domain': X_deny })
        # add labels
        df_allow['label'] = label['top']
        df_deny['label'] = label['bad']

        # determine length for ballanced validation and test sets
        df_length = min(len(df_allow), len(df_deny))

        # ballanced test set
        test_deny = df_deny.sample(n=int(df_length * 0.2), random_state=random_state)
        df_deny = df_deny.drop(test_deny.index)

        test_allow = df_allow.sample(n=len(test_deny), random_state=random_state)
        df_allow = df_allow.drop(test_allow.index)

        test = pd.concat([test_deny, test_allow], ignore_index=True)
        test = test.sample(frac=1, random_state=random_state)

        # ballanced validation set
        valid_deny = df_deny.sample(n=int(df_length * 0.2), random_state=random_state)
        df_deny = df_deny.drop(valid_deny.index)

        valid_allow = df_allow.sample(n=len(valid_deny), random_state=random_state)
        df_allow = df_allow.drop(valid_allow.index)

        validate = pd.concat([valid_deny, valid_allow], ignore_index=True)
        validate = validate.sample(frac=1, random_state=random_state)

        # train set
        train = pd.concat([df_deny, df_allow], ignore_index=True)
        train = train.sample(frac=1, random_state=random_state)

        # return: X_train, y_train, X_validate, y_validate, X_test, y_test
        return (
            train['domain'].to_numpy(),
            train['label'].to_numpy(),
            validate['domain'].to_numpy(),
            validate['label'].to_numpy(),
            test['domain'].to_numpy(),
            test['label'].to_numpy(),
        )

    def _get_tld(domain):
        """
        Get valid top level domain.

        Arguments
        ---------
        domain : str
            Domain string.

        Returns
        -------
        str or None
            Valid top level domain or None.
        """
        return get_tld(f"http://{domain}", fix_protocol=True, fail_silently=True)

    def _has_a_correct_tld(domain):
        """
        Validate top level domain.

        Arguments
        ---------
        domain : str
            Domain string.

        Returns
        -------
        bool
            True if top level domain is valid; otherwise False.
        """
        return get_fld(f"http://{domain}", fix_protocol=True, fail_silently=True) is not None

    def _not_a_top_level_domain(df):
        """
        Get dataset only with valid top level domains.

        Arguments
        ---------
        df : pd.DataFrame
            Data frame with at least one column named "domain".

        Returns
        -------
        pd.DataFrame
            Input data frame with only valid top level domains.
        """
        return df[df['domain'].apply(DataSelection._has_a_correct_tld)]

    ###############################################################################################
    # PUBLIC ##############################################

    def whitelist_blacklist(
        allow_csv="./datasets/whitelist.txt",
        deny_csv="./datasets/blacklist.txt",
        dga_csv=None,
    ):
        """
        Use balanced white- and blacklist.

        Arguments
        ---------
        allow_csv : str, optional
            File path to whitelist.
        deny_csv : str, optional
            File path to blacklist.
        dga_csv : str, optional
            Not used here.

        Returns
        -------
        6-Tuple of np.ndarray
            X_train, y_train, X_validate, y_validate, X_test, y_test
        """
        # load lists
        df_allow = pd.read_csv(allow_csv, header=None, names=['domain'])
        df_deny = pd.read_csv(deny_csv, header=None, names=['domain'])

        # cut all domains which does not have a correct tld
        df_allow = DataSelection._not_a_top_level_domain(df_allow)
        df_deny = DataSelection._not_a_top_level_domain(df_deny)

        # get the same amount of data for both lists
        # avoid imbalanced datasets
        min_count = min(df_deny['domain'].count(), df_allow['domain'].count())
        df_allow = df_allow[:min_count]
        df_deny = df_deny[:min_count]

        return DataSelection._split_merge_shuffle(df_allow['domain'], df_deny['domain'])

    def whitelist_blacklist_unbalanced(
        allow_csv="./datasets/whitelist.txt",
        deny_csv="./datasets/blacklist.txt",
        dga_csv=None,
    ):
        """
        Use unbalanced white- and blacklist.

        Arguments
        ---------
        allow_csv : str, optional
            File path to whitelist.
        deny_csv : str, optional
            File path to blacklist.
        dga_csv : str, optional
            Not used here.

        Returns
        -------
        6-Tuple of np.ndarray
            X_train, y_train, X_validate, y_validate, X_test, y_test
        """
        # load lists
        df_allow = pd.read_csv(allow_csv, header=None, names=['domain'])
        df_deny = pd.read_csv(deny_csv, header=None, names=['domain'])

        # cut all domains which does not have a correct tld
        df_allow = DataSelection._not_a_top_level_domain(df_allow)
        df_deny = DataSelection._not_a_top_level_domain(df_deny)

        return DataSelection._split_merge_shuffle(df_allow['domain'], df_deny['domain'])


    def whitelist_blacklist_cutted(
        allow_csv="./datasets/whitelist.txt",
        deny_csv="./datasets/blacklist.txt",
        dga_csv=None,
        df_length=100000,
    ):
        """
        Use balanced white- and blacklist.

        Arguments
        ---------
        allow_csv : str, optional
            File path to whitelist.
        deny_csv : str, optional
            File path to blacklist.
        dga_csv : str, optional
            Not used here.
        df_length : int, optional
            White-/blacklist cut length.

        Returns
        -------
        6-Tuple of np.ndarray
            X_train, y_train, X_validate, y_validate, X_test, y_test
        """
        # load lists
        df_allow = pd.read_csv(allow_csv, header=None, names=['domain'])
        df_deny = pd.read_csv(deny_csv, header=None, names=['domain'])

        # cut all domains which does not have a correct tld
        df_allow = DataSelection._not_a_top_level_domain(df_allow)
        df_deny = DataSelection._not_a_top_level_domain(df_deny)

        # get the same amount of data for both lists
        # avoid imbalanced datasets
        df_allow = df_allow[:df_length]
        df_deny = df_deny[:df_length]

        return DataSelection._split_merge_shuffle(df_allow['domain'], df_deny['domain'])

    def whitelist_blacklist_and_dga(
        allow_csv="./datasets/whitelist.txt",
        deny_csv="./datasets/blacklist.txt",
        dga_csv="./datasets/data/dgas_preprocessed.csv",
    ):
        """
        Use unbalanced white- and blacklist and domain generator algorithm list as additional blacklist entities.

        Arguments
        ---------
        allow_csv : str, optional
            File path to whitelist.
        deny_csv : str, optional
            File path to blacklist.
        dga_csv : str, optional
            File path to domain generator algorithm (dga) csv.

        Returns
        -------
        6-Tuple of np.ndarray
            X_train, y_train, X_validate, y_validate, X_test, y_test
        """
        random_state = get_seed()

        # load lists
        df_allow = pd.read_csv(allow_csv, header=None, names=['domain'])
        df_deny = pd.read_csv(deny_csv, header=None, names=['domain'])
        df_dgas = pd.read_csv(dga_csv, header=None, names=['domain'])

        # cut all domains which does not have a correct tld
        df_allow = DataSelection._not_a_top_level_domain(df_allow)
        df_deny = DataSelection._not_a_top_level_domain(df_deny)
        df_dgas = DataSelection._not_a_top_level_domain(df_dgas)

        # merge df_deny and df_dgas and shuffle
        df_deny = shuffle([*df_deny['domain'], *df_dgas['domain']], random_state=random_state)
        df_deny = pd.DataFrame(df_deny, columns=['domain'])

        return DataSelection._split_merge_shuffle(df_allow['domain'], df_deny['domain'])

    def whitelist_blacklist_and_dga_cutted(
        allow_csv="./datasets/whitelist.txt",
        deny_csv="./datasets/blacklist.txt",
        dga_csv="./datasets/data/dgas_preprocessed.csv",
        allow_length=100000,
    ):
        """
        Use unbalanced white- and blacklist and domain generator algorithm list as additional blacklist entities.
        Additionally cut whitelist to the length of allow_length.

        Arguments
        ---------
        allow_csv : str, optional
            File path to whitelist.
        deny_csv : str, optional
            File path to blacklist.
        dga_csv : str, optional
            File path to domain generator algorithm (dga) csv.
        allow_length : int, optional
            Whitelist cut length.

        Returns
        -------
        6-Tuple of np.ndarray
            X_train, y_train, X_validate, y_validate, X_test, y_test
        """
        random_state = get_seed()

        # load lists
        df_allow = pd.read_csv(allow_csv, header=None, names=['domain'])
        df_deny = pd.read_csv(deny_csv, header=None, names=['domain'])
        df_dgas = pd.read_csv(dga_csv, header=None, names=['domain'])

        # cut all domains which does not have a correct tld
        df_allow = DataSelection._not_a_top_level_domain(df_allow)
        df_deny = DataSelection._not_a_top_level_domain(df_deny)

        # merge df_deny and df_dgas and shuffle
        df_deny = shuffle([*df_deny['domain'], *df_dgas['domain']], random_state=random_state)
        df_deny = pd.DataFrame(df_deny, columns=['domain'])

        # cut whitelist
        df_allow = df_allow[:allow_length]

        return DataSelection._split_merge_shuffle(df_allow['domain'], df_deny['domain'])
