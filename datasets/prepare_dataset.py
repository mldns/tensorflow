#!/usr/bin/env python3
############################################################
# Prepare black and white lists.
############################################################
# Preparation tasks:
# - merge multiple lists
# - remove duplicates
# - save prepared data in {black,white}list.txt
############################################################
if __name__ == "__main__":
    from os import path
    import sys

    sys.path.append(path.join(path.dirname(__file__), ".."))
############################################################

import pandas as pd
import tarfile
import tempfile

from argparse import ArgumentParser
from utils.argparser_ext import directory, file, str2bool

from urllib.request import Request, urlopen, urlretrieve
from csv import QUOTE_NONE as CSV_QUOTE_NONE


def prepare_df(df, label=None, substitute=False):
    """
    Prepare data frame.

    - drop missing values
    - drop duplicate values
    - optional: add label column
    - optional: substitute special charaters to split dns name into words

    Args:
      df: data frame
      label: label to add (Default: None)
      substitute: substitute special charaters to split dns name into words (Default: False)

    Returns:
      Prepared data frame.

    """

    def prepare_name(name):
        """
        Prepare name by substituting special characters.

        Args:
          name: string to prepare

        Returns:
          Prepared Name
          Example:
          Example: 'google.com'   ->  'google com'
          Example: 'google.com'   ->  'google com'
          Example: 'google.com'   ->  'google com'
          'x-y_z.co.uk'  ->  'x y z co uk'

        """
        return name.replace(".", " ").replace("-", " ").replace("_", " ")

    m_df = df
    m_df = m_df[["name"]]

    if label is not None:
        m_df["label"] = label

    m_df = m_df.dropna()
    m_df = m_df.drop_duplicates("name")

    if substitute:
        m_df["name"] = m_df["name"].apply(prepare_name)

    return m_df


def prepare_whitelist(labeled=False, substitute=False):
    """
    Download, prepare and save whitelist.

    Args:
      whitelist_path: file path to save whitelist
      labeled: add label to whitelist (Default value = False)
      substitute: substitute special charaters to split dns name into words (Default value = False)

    Returns:
        Top Domains and columns names

    """
    url = "http://s3-us-west-1.amazonaws.com/umbrella-static/top-1m.csv.zip"
    top_df = pd.read_csv(url, header=None, names=["rank", "name"], usecols=[1])
    top_df = prepare_df(top_df, "__label__top" if labeled else None, substitute)
    columns = ["label", "name"] if labeled else ["name"]

    return top_df, columns


def prepare_blacklist(labeled=False, substitute=False):
    """
    Download, prepare and save blacklist.

    Args:
      blacklist_path: file path to save blacklist
      labeled: add label to blacklist (Default value = False)
      substitute: substitute special charaters to split dns name into words (Default value = False)

    Returns:
        Bad Domains and columns names
    """
    bad_dfs = []

    # 1. blacklist: COVID-19 Cyber Threat Coalition
    # workaround since site does not accept Python requests, only Browser
    url = "https://blocklist.cyberthreatcoalition.org/vetted/domain.txt"
    req = Request(url)
    req.add_header(
        "User-Agent",
        "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:77.0) Gecko/20100101 Firefox/77.0",
    )
    content = urlopen(req)
    bad_dfs.append(
        pd.read_csv(content, header=None, comment="#", names=["name"], delimiter=" ")
    )

    # 2. blacklist: StevenBlack
    url = "https://raw.githubusercontent.com/StevenBlack/hosts/master/hosts"
    bad_dfs.append(
        pd.read_csv(
            url,
            header=None,
            comment="#",
            names=["IP", "name"],
            delimiter=" ",
            usecols=[1],
            skiprows=30,
        )
    )

    # 3. blacklist: Université Toulouse 1 Capitole
    labels = [
        ("malware", "phishing/domains"),
        ("phishing", "phishing/domains"),
    ]
    for label, archive_path in labels:
        url = f"http://dsi.ut-capitole.fr/blacklists/download/{label}.tar.gz"
        archive = f"{tempfile.gettempdir()}/{label}.tar.gz"
        urlretrieve(url, archive)
        with tarfile.open(archive, "r") as tar:
            bad_dfs.append(
                pd.read_csv(
                    tar.extractfile(archive_path),
                    error_bad_lines=False,
                    header=None,
                    comment="#",
                    names=["name"],
                    usecols=[0],
                )
            )

    # concat and prepare
    bad_df = pd.concat(bad_dfs, ignore_index=True)
    bad_df = prepare_df(bad_df, "__label__bad" if labeled else None, substitute)
    columns = ["label", "name"] if labeled else ["name"]
    return bad_df, columns


def main(config):
    """
    Main entry for the prepare dataset script.

    Args:
      config: run configurations

    Returns:

    """
    top_df, top_columns_names = prepare_whitelist(
        config.labeled,
        config.substitute_special_charaters,
    )
    bad_df, bad_columns_names = prepare_blacklist(
        config.labeled,
        config.substitute_special_charaters,
    )

    common = top_df["name"].isin(bad_df["name"])
    print(
        "Common in both top and bad:", len(top_df[common == True]), "(removed in top)"
    )
    top_df = top_df[common == False]

    print("top:", len(top_df))
    print("bad:", len(bad_df))

    top_df[top_columns_names].to_csv(
        path.join(config.out_dir, config.whitelist),
        index=False,
        header=False,
        quoting=CSV_QUOTE_NONE,
    )

    bad_df[bad_columns_names].to_csv(
        path.join(config.out_dir, config.blacklist),
        index=False,
        header=False,
        quoting=CSV_QUOTE_NONE,
    )


def parseArgs():
    """
    Parse command-line arguments.

    Args:

    Returns:
      Run configuration.

    """
    parser = ArgumentParser(description="{Black,White}list preparation")

    parser.add_argument(
        "--out-dir",
        type=directory(),
        default=path.dirname(__file__),
        help="prepared dataset directory",
    )

    parser.add_argument(
        "--whitelist",
        type=str,
        default="whitelist.txt",
        help="whitelist name",
    )

    parser.add_argument(
        "--blacklist",
        type=str,
        default="blacklist.txt",
        help="blacklist name",
    )

    parser.add_argument(
        "--labeled",
        type=str2bool,
        const=True,
        default=False,
        nargs="?",
        help="add labels to black- and whitelist",
    )

    parser.add_argument(
        "--substitute-special-characters",
        type=str2bool,
        const=True,
        default=False,
        nargs="?",
        help="substitute special charaters to split dns name into words",
    )

    config = parser.parse_args()
    return config


if __name__ == "__main__":
    main(parseArgs())
