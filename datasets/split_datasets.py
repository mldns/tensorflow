#!/usr/bin/env python3
############################################################
# Split datasets.
############################################################
# Requirements:
# - black- and whitelist exists
############################################################
# Steps
# - load black- and whitelist
# - adjust list sizes (some amount of entries)
# - create and save {train,validation,test}-datasets
#   radio will be:   60%   20%        20%
############################################################
if __name__ == "__main__":
    from os import path
    import sys

    sys.path.append(path.join(path.dirname(__file__), ".."))
############################################################

import pandas as pd
import numpy as np

from argparse import ArgumentParser
from utils.argparser_ext import directory, file, str2bool

from csv import QUOTE_NONE as CSV_QUOTE_NONE


def main(config):
    """
    Main entry for the prepare dataset script.

    Args:
      config: run configurations

    Returns:

    """
    # load lists
    top_df = pd.read_csv(
        config.whitelist, header=None, names=["name"], usecols=[config.data_column]
    )
    bad_df = pd.read_csv(
        config.blacklist, header=None, names=["name"], usecols=[config.data_column]
    )

    # cut of whitelist to get the same amount of data for both lists
    if not config.imbalanced:
        top_df = top_df[: bad_df["name"].count()]

    # add labels
    top_df["label"] = "__label__top"
    bad_df["label"] = "__label__bad"

    # merge, shuffle, split
    # 60% - train set,
    # 20% - validation set,
    # 20% - test set

    length_of_bad = len(bad_df)
    test_bad = bad_df.sample(n=int(length_of_bad * 0.2), random_state=42)
    bad_df = bad_df.drop(test_bad.index)

    test_top = top_df.sample(n=len(test_bad), random_state=42)
    top_df = top_df.drop(test_top.index)

    test = pd.concat([test_bad, test_top], ignore_index=True)
    test = test.sample(frac=1, random_state=42)

    valid_bad = bad_df.sample(n=int(length_of_bad * 0.2), random_state=42)
    bad_df = bad_df.drop(valid_bad.index)

    valid_top = top_df.sample(n=len(valid_bad), random_state=42)
    top_df = top_df.drop(valid_top.index)

    validate = pd.concat([valid_bad, valid_top], ignore_index=True)
    validate = validate.sample(frac=1, random_state=42)

    train = pd.concat([bad_df, top_df], ignore_index=True)
    train = train.sample(frac=1, random_state=42)

    print("Train, Validate, Test Sizes")
    print(len(train), len(validate), len(test))

    # save
    train[["label", "name"]].to_csv(
        path.join(config.out_dir, config.train),
        index=False,
        header=False,
        quoting=CSV_QUOTE_NONE,
    )
    validate[["label", "name"]].to_csv(
        path.join(config.out_dir, config.validate),
        index=False,
        header=False,
        quoting=CSV_QUOTE_NONE,
    )
    test[["label", "name"]].to_csv(
        path.join(config.out_dir, config.test),
        index=False,
        header=False,
        quoting=CSV_QUOTE_NONE,
    )


def parseArgs():
    """
    Parse command-line arguments.

    Args:

    Returns:
      Run configuration.

    """
    parser = ArgumentParser(description="Create Datasets (train, validate, test)")

    parser.add_argument(
        "--out-dir",
        type=directory(),
        default=path.dirname(__file__),
        help="prepared dataset directory",
    )

    parser.add_argument(
        "--whitelist",
        type=file,
        default=path.join(path.dirname(__file__), "whitelist.txt"),
        help="whitelist file path",
    )

    parser.add_argument(
        "--blacklist",
        type=file,
        default=path.join(path.dirname(__file__), "blacklist.txt"),
        help="blacklist file path",
    )

    parser.add_argument(
        "--train",
        type=str,
        default="train.txt",
        help="training dataset file name",
    )

    parser.add_argument(
        "--validate",
        type=str,
        default="validate.txt",
        help="validation dataset file name",
    )

    parser.add_argument(
        "--test",
        type=str,
        default="test.txt",
        help="test dataset file name",
    )

    parser.add_argument(
        "--data-column",
        type=int,
        default=0,
        help="csv data column (zero-based)",
    )

    parser.add_argument(
        "--imbalanced",
        type=str2bool,
        const=True,
        default=False,
        nargs="?",
        help="do not trim datasets to the same size",
    )

    config = parser.parse_args()
    return config


if __name__ == "__main__":
    main(parseArgs())
