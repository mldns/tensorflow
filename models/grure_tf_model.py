#!/usr/bin/env python3

import tensorflow as tf
from tensorflow.keras import layers
from tensorflow.keras import regularizers
from kerastuner.engine.hyperparameters import HyperParameters

def get_model_hp(hp:HyperParameters, encoder):
    """
    Compiles and returns the model

    Args:
      encoder: textvectorization layer which has the tokenizer
      hp:HyperParameters: HyperParameters from keras-tuner

    Returns:
      model: compiled model

    """

    model = tf.keras.Sequential(
        [
            tf.keras.Input(shape=(1,), dtype=tf.string),
            encoder,
        ]
    )

    embedding_dim_max = 128
    hp_embedding_dim = hp.Int('embedding_dim', min_value=2, max_value=embedding_dim_max, step=16)
    model.add(tf.keras.layers.Embedding(
                len(encoder.get_vocabulary()), hp_embedding_dim, mask_zero=True))

    hp_with_second_gru = hp.Choice("with additional gru layer", [False, True])
    hp_dropout_1 = hp.Float("dropout_1", min_value= 0.1, max_value=0.9, step= 0.1)
    model.add(layers.GRU(hp_embedding_dim, dropout=hp_dropout_1, return_sequences=hp_with_second_gru))

    if hp_with_second_gru:
        hp_gru_2_units = hp.Int('gru_2_units', min_value=8, max_value=128, step=32)
        hp_dropout_2 = hp.Float("dropout_2", min_value= 0.1, max_value=0.9, step= 0.1)
        model.add(layers.GRU(hp_gru_2_units, dropout=hp_dropout_2))


    if hp.Choice("dense_with_regularizers", [False, True]):
        dense_units = hp.Int("dense_reg_units", min_value=2, max_value=32, step=8)
        dense_reg_l2 = hp.Choice("dense_reg_l2", [0.001, 0.01, 0.1])
        model.add(layers.Dense(dense_units, kernel_regularizer=regularizers.l2(dense_reg_l2)))

    model.add(layers.Dense(1))
    hp_learning_rate = hp.Choice('learning_rate', values=[1e-2, 1e-3, 1e-4])
    model.compile(
        loss=tf.losses.BinaryCrossentropy(from_logits=True),
        optimizer=tf.keras.optimizers.Adam(hp_learning_rate),
        metrics=tf.metrics.BinaryAccuracy(threshold=0.0),
    )
    return model


def get_model(encoder, embedding_dim=100, learning_rate=0.001):
    """
    Compiles and returns the model

    Args:
      encoder: textvectorization layer which has the tokenizer
      embedding_dim: Dimension size of the Embedding Layer(Default value = 100)
      learning_rate: The learning rate which should be used (Default value = 0.001)

    Returns:
      model: compiled model

    """

    model = tf.keras.Sequential()
    model.add(encoder)

    model.add(
        tf.keras.layers.Embedding(
            len(encoder.get_vocabulary()), embedding_dim, mask_zero=True
        )
    )
    model.add(layers.GRU(100, dropout=0.9, return_sequences=True))
    model.add(layers.GRU(100, dropout=0.9))
    model.add(layers.Dense(32, kernel_regularizer=regularizers.l2(0.001)))
    model.add(layers.Dropout(0.5))
    model.add(layers.Dense(1))
    model.compile(
        loss=tf.keras.losses.BinaryCrossentropy(from_logits=True),
        optimizer=tf.keras.optimizers.Adam(learning_rate),
        metrics=tf.metrics.BinaryAccuracy(threshold=0.0),
    )
    return model


if __name__ == "__main__":
    from os import path
    import sys

    sys.path.append(path.join(path.dirname(__file__), ".."))
