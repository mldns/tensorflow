#!/usr/bin/env python3

import tensorflow as tf
from kerastuner.engine.hyperparameters import HyperParameters

def get_model_hp(hp:HyperParameters, encoder):
    """
    Compiles and returns the model

    Args:
      encoder: textvectorization layer which has the tokenizer
      hp:HyperParameters: HyperParameters from keras-tuner

    Returns:
      model: compiled model

    """

    model = tf.keras.Sequential(
        [
            tf.keras.Input(shape=(1,), dtype=tf.string),
            encoder,
        ]
    )

    embedding_dim_max = 128
    hp_embedding_dim = hp.Int('embedding_dim', min_value=2, max_value=embedding_dim_max, step=16)
    model.add(tf.keras.layers.Embedding(
                len(encoder.get_vocabulary()), hp_embedding_dim, mask_zero=True))

    model.add(tf.keras.layers.Bidirectional(
                tf.keras.layers.LSTM(hp_embedding_dim, return_sequences=True)
            ))

    hp_embedding_dim_2 = hp.Int('embedding_dim_2', min_value=2, max_value=embedding_dim_max//2, step=16)
    model.add(tf.keras.layers.Bidirectional(tf.keras.layers.LSTM(hp_embedding_dim_2)))


    if hp.Choice("with additional dense layer", [False, True]):
        hp_dense = hp.Int('dense', min_value=2, max_value=32)
        model.add(tf.keras.layers.Dense(hp_dense, activation="relu"))

    if hp.Choice("with_dropout", [False, True]):
        hp_dropout = hp.Float("dropout", min_value= 0.1, max_value=0.9, step= 0.1)
        model.add(tf.keras.layers.Dropout(hp_dropout))
    model.add(tf.keras.layers.Dense(1))


    hp_learning_rate = hp.Choice('learning_rate', values=[1e-2, 1e-3, 1e-4])
    model.compile(
        loss=tf.losses.BinaryCrossentropy(from_logits=True),
        optimizer=tf.keras.optimizers.Adam(hp_learning_rate),
        metrics=tf.metrics.BinaryAccuracy(threshold=0.0),
    )
    return model

def get_model(encoder, embedding_dim=100, learning_rate=0.001):
    """
    Compiles and returns the model

    Args:
      encoder: textvectorization layer which has the tokenizer
      embedding_dim: Dimension size of the Embedding Layer(Default value = 100)
      learning_rate: The learning rate which should be used (Default value = 0.001)

    Returns:
      model: compiled model

    """

    model = tf.keras.Sequential(
        [
            encoder,
            tf.keras.layers.Embedding(
                len(encoder.get_vocabulary()), embedding_dim, mask_zero=True
            ),
            tf.keras.layers.Bidirectional(
                tf.keras.layers.LSTM(embedding_dim, return_sequences=True)
            ),
            tf.keras.layers.Bidirectional(
                tf.keras.layers.LSTM(int(embedding_dim // 2))
            ),
            tf.keras.layers.Dense(embedding_dim // 4, activation="relu"),
            tf.keras.layers.Dropout(0.5),
            tf.keras.layers.Dense(1),
        ]
    )
    model.compile(
        loss=tf.keras.losses.BinaryCrossentropy(from_logits=True),
        optimizer=tf.keras.optimizers.Adam(learning_rate),
        metrics=tf.metrics.BinaryAccuracy(threshold=0.0),
    )
    return model


if __name__ == "__main__":
    from os import path
    import sys

    sys.path.append(path.join(path.dirname(__file__), ".."))
