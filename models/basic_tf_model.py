#!/usr/bin/env python3

import tensorflow as tf
from tensorflow.keras import layers
from tensorflow.keras import losses
from kerastuner.engine.hyperparameters import HyperParameters
from kerastuner import HyperModel


class Basic_Model(tf.keras.Model):
    """This is a class for the Basic_Model"""

    def get_config(self):
        """ """
        pass

    def __init__(self, embedding_dim=32, max_features=1000):
        """
        Initialize the Layers of the model

        Args:
          embedding_dim: the dimension size of the embedding layer
          max_features: Size of the Vocabulary, which should be embedded

        Returns:


        """
        super().__init__()

        self.embedding_dim = embedding_dim
        self.max_features = max_features

        self.embedding = layers.Embedding(self.max_features + 1, self.embedding_dim)
        self.dropout1 = layers.Dropout(0.2)
        self.pooling = layers.GlobalAveragePooling1D()
        self.dropout2 = layers.Dropout(0.2)
        self.out = layers.Dense(1, activation="sigmoid")

    def call(self, inputs, **kwargs):
        """
        calls each layer and generates an output

        Args:
          inputs: text (domain)
          **kwargs:

        Returns:
          Prediction for the domain

        """
        x = self.embedding(inputs)
        x = self.dropout1(x)
        x = self.pooling(x)
        x = self.dropout2(x)
        return self.out(x)


class BasicHyperModel(HyperModel):
    def __init__(self, encoder):
        self.encoder = encoder

    def build(self, hp):
        model = tf.keras.Sequential(
            [
                tf.keras.Input(shape=(1,), dtype=tf.string),
                self.encoder,
            ]
        )
        hp_embedding_dim = hp.Int('embedding_dim', min_value=2, max_value=128, step=16)
        model.add(tf.keras.layers.Embedding(
                    len(self.encoder.get_vocabulary()), hp_embedding_dim, mask_zero=True
                ))
        hp_dropout_1 = hp.Float("dropout_1", min_value= 0.1, max_value=0.9, step= 0.1)
        model.add(layers.Dropout(hp_dropout_1))
        model.add(layers.GlobalAveragePooling1D())

        if hp.Choice("with_second_dropout", [False, True], default=True):
            hp_dropout_2 = hp.Float("dropout_2", min_value= 0.1, max_value=0.9, step= 0.1)
            model.add(layers.Dropout(hp_dropout_2))
        model.add(layers.Dense(1))

        hp_learning_rate = hp.Choice('learning_rate', values=[1e-2, 1e-3, 1e-4])
        model.compile(
            loss=losses.BinaryCrossentropy(from_logits=True),
            optimizer=tf.keras.optimizers.Adam(hp_learning_rate),
            metrics=tf.metrics.BinaryAccuracy(threshold=0.0),
        )
        return model



def get_model_hp(hp:HyperParameters, encoder):
    """
    Compiles and returns the model

    Args:
      encoder: textvectorization layer which has the tokenizer
      hp:HyperParameters: HyperParameters from keras-tuner

    Returns:
      model: compiled model

    """
    # model = Basic_Model(embedding_dim=embedding_dim, max_features=max_features)
    model = tf.keras.Sequential(
        [
            tf.keras.Input(shape=(1,), dtype=tf.string),
            encoder,
        ]
    )
    hp_embedding_dim = hp.Int('embedding_dim', min_value=2, max_value=128, step=16)
    model.add(tf.keras.layers.Embedding(
                len(encoder.get_vocabulary()), hp_embedding_dim, mask_zero=True
            ))
    hp_dropout_1 = hp.Float("dropout_1", min_value= 0.1, max_value=0.9, step= 0.1)
    model.add(layers.Dropout(hp_dropout_1))
    model.add(layers.GlobalAveragePooling1D())

    if hp.Choice("with_second_dropout", [False, True], default=True):
        hp_dropout_2 = hp.Float("dropout_2", min_value= 0.1, max_value=0.9, step= 0.1)
        model.add(layers.Dropout(hp_dropout_2))
    model.add(layers.Dense(1))

    hp_learning_rate = hp.Choice('learning_rate', values=[1e-2, 1e-3, 1e-4])
    model.compile(
        loss=losses.BinaryCrossentropy(from_logits=True),
        optimizer=tf.keras.optimizers.Adam(hp_learning_rate),
        metrics=tf.metrics.BinaryAccuracy(threshold=0.0),
    )
    return model

def get_model(encoder, embedding_dim=100, learning_rate=0.001):
    """
    Compiles and returns the model

    Args:
      encoder: textvectorization layer which has the tokenizer
      embedding_dim: Dimension size of the Embedding Layer(Default value = 100)
      learning_rate: The learning rate which should be used (Default value = 0.001)

    Returns:
      model: compiled model

    """
    # model = Basic_Model(embedding_dim=embedding_dim, max_features=max_features)

    model = tf.keras.Sequential(
        [
            tf.keras.Input(shape=(1,), dtype=tf.string),
            encoder,
            tf.keras.layers.Embedding(
                len(encoder.get_vocabulary()), embedding_dim, mask_zero=True
            ),
            layers.Dropout(0.2),
            layers.GlobalAveragePooling1D(),
            layers.Dropout(0.2),
            layers.Dense(1),
        ]
    )

    model.compile(
        loss=losses.BinaryCrossentropy(from_logits=True),
        optimizer=tf.keras.optimizers.Adam(learning_rate),
        metrics=tf.metrics.BinaryAccuracy(threshold=0.0),
    )
    return model


if __name__ == "__main__":
    from os import path
    import sys

    sys.path.append(path.join(path.dirname(__file__), ".."))

    model = get_model()
    model.summary()
