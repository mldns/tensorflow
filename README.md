# MLDNS - TensorFlow

This repository is part of the [Machine Learning DNS (MLDNS)](https://gitlab.com/mldns/mldns) research project and mainly focuses on the machine learning aspects to classify domain names into benign and malicious.

The report to this repository can be found [here](https://gitlab.com/mldns/ml-report).

The developer documentation is available [here](https://mldns.gitlab.io/tensorflow).


## Introduction

Home network security is a big problem that is not easy to solve. Every year, users are affected
by various attacks. Most of these attacks can be mitigated by blocking malicious domains.
Currently, all unknown domains are considered benign accepted. This is precisely where we want
to intervene.

Our research target is to determine whether machine learning can be used to distinguish benign
and malicious domains based on its name. We want to show that it is possible to train a model
that is small and efficient enough to be put into a home router by default.

Surprisingly, our experiments clearly show that an appropriate classification based only on the domain name
is very promising. Depending on the machine learning tool, the resulting models are also
relative small and efficient, so that they can be easily integrated into any home router.

In this project different models were trained using [ktrain](https://github.com/amaiya/ktrain) and [TensorFlow](https://www.tensorflow.org/).

For the models trained/learned with [ktrain](https://github.com/amaiya/ktrain) the name of the model is marked with the suffix `klearn` and for [TensorFlow](https://www.tensorflow.org/) with the suffix `tf`.

For example: `distilbert_klearn_model` and `basic_tf_model`


## Installation

### Requirements

- virtualenv
  ```bash
  # install pip
  sudo apt install python3-pip
  # install virtualenv
  python3 -m pip install virtualenv

  # if you have some trouble to install the virtualenv on a debian machine you might try
  sudo apt install virtualenv
  ```
- python version 3.8 needs to be installed


### Installation

Make sure you have Python 3.8 installed, otherwise the following commands will not work.

Execute the following commands

```bash
virtualenv -p $(which python3.8) .venv
source .venv/bin/activate
pip install -U pip
pip install -r "requirements.txt"
```


#### Data
Before you can train the models, you need to make sure that you run the following script or commands so that all datasets are downloaded and prepared.
If you also want to use a DGA list to remove DGA domains out of the datasets, move your file to `./datasets/data/dgas_preprocessed.csv` before running the rest of the commands.  
The preprocessing for the DGA list is the replacing of `.` , `-`, `__` with a space character.

Further below you will find brief information about the data and its origin.
For more information and a deeper understanding, you should take a look at the report, which you can find [here](https://gitlab.com/mldns/ml-report).

The downloading and preprocessing can take some time (several minutes).

Execute this script:
```bash
./scripts/make_data.sh
```

or do it manually by:
```bash
mkdir -p "./datasets/data"
./scripts/download_and_prepare_white_and_blacklist.py --out-dir "./datasets/data"
./scripts/advanced_whitelist_cleaning.py
./scripts/prepare_split_datasets.py
cp ./datasets/data-prepared-cleaned/whitelist_blacklist/{test,train,validate}.txt ./datasets/
```

### Whitelist Data
The Cisco Umbrella Popularity List.  
This list consists of the most popular 1
million domains, which are the most queried domains in the Umbrella global network.  
https://s3-us-west-1.amazonaws.com/umbrella-static/index.html

### Blacklist Data
- COVID-19 Cyber Threat Coalition Blocklist  
Blocklist of domains which do malicious or criminal activies in the COVID-19 pandemic.  
The complete blacklist can be found here:
https://blocklist.cyberthreatcoalition.org/vetted/domain.txt
- Steven Black’s Hosts file  
uses the most basic variant Unified hosts which includes adware and malware.
The full list of included sources can be found here:
https://github.com/StevenBlack/hosts#sources-of-hosts-data-unified-in-this-variant  
The complete blacklist can be found here:
https://raw.githubusercontent.com/StevenBlack/hosts/master/hosts
- Universite Toulouse 1 Capitolé Blacklist  
This project uses the categories malware and phishing.  
All blacklists sorted into multiple categories can be found here:
http://dsi.ut-capitole.fr/blacklists/index_en.php

### Datasets
This projects uses the above domain lists to generate three datasets.

Datasets:
- whitelist_blacklist  - get the same amount of data for both whitelist and blacklist (as much as the minimum of both lists)
- whitelist_blacklist_unbalanced - take as many of each whitelist and blacklist as possible (unbalanced)
- whitelist_blacklist_cutted - get the same amount of data of both lists but cutted to a value default: 100,000

The following preprocess steps are used.

- remove duplicate domains,
- remove invalid top level domains (TLDs),
- remove domains that are also part of the blacklist,
- remove local domain addresses, e.g. localhost,
- remove domains that include UUID as a significant part of the domain,
- remove domains that include subdomains with more than 30 characters

To use a different dataset you have to set the `--data` option of `main.py` ([parameters](#parameters)).

There is also an option to remove DGA (Domain Generation Algorithm) domains.  
For this you need a DGA list which you preprocess by replacing the `_` , `.` , `-` with a space character.  
You place this preprocessed file in `./datasets/dgas_preprocessed.csv` before executing `advanced_whitelist_cleaning.py`.

For Datasets with DGA you can choose one of the following options.
- whitelist_blacklist_and_dga - same as whitelist_blacklist but with removing dga domains
- whitelist_blacklist_and_dga_cutted - same as whitelist_blacklist_cutted but with removing dga domains


## Train models
In order to train models, you will use the `main.py` file. If you need help, you can call the help with `python main.py --help`

The most important argument is `--trainer`, which specifies which trainer and therefore which model is to be trained.

Here you can find a list of all trainers:

- basic_tf
- bi_tf
- bigru_klearn
- distilbert_klearn
- gru_klearn
- gru_tf
- grure_tf
- fasttext_klearn
- fasttext_tf

Be sure to specify the name as shown above:
```bash
python main.py --trainer basic_tf
```
> **NOTE: Some of these models can take many hours to train.**

If you want to select another dataset, you can do so with the `--data` argument, see [datasets](#datasets) for more information.

For example:
```bash
python main.py --trainer basic_tf --data whitelist_blacklist_unbalanced
```

In this project, all models and evaluations are stored using [MLflow](https://github.com/mlflow/mlflow/).

To access these results, run `mlflow ui`.
The server address of mlflow is displayed in the terminal, the default value is `http://127.0.0.1:5000`.
All metrics given the suffix `_test` are made by using the test dataset.

Additional parameters can be seen below [parameters](#parameters).

### Train all models

If you would like to train all models, you can run the following script:
```bash
./scripts/train_all_models.sh
```

### Interactive/Try out

You can try the models interactively by using the `./evaluation/interactive.py` script.

This script requires the `--path` parameter, which should point to the directory of the trained model you want to test.

For example:
```bash
./evaluation/interactive.py --path mlruns/1/aa21e27cf05841edb69aec24b93843dc/artifacts/basic_tf_model

```

It will look like:
```bash
Check domain (Press Enter to leave): google.com
Predicted: Benign domain with probability 0.9947433471679688
Check domain (Press Enter to leave): mqqvrwdsehrcicki-dot-millinium.ey.r.appspot.com
Predicted: Malicious domain with probability 0.9998060991783859
Check domain (Press Enter to leave):
Bye
```



## Evaluation
The evaluation can be found in mlflow by using `mlflow ui` in the project directory while using the virtual environment with
`source .venv/bin/activate` in the same directory.

Additional you have the possibility to create the evaluation **again** by using the following scripts:
- `evaluation.py`
- `evaluation_k.py`

The `--model` flag is used to evaluate the last trained model in the `trained_models` directory.
If you want to use another model or the models from the `mlruns` directory, you can use the `--path` parameter instead.

like:
```bash
python ./evaluation/evaluation.py --path mlruns/1/c9783be839fb4f57b0953214dab792da/artifacts/basic_tf_model
```

or:
```bash
python ./evaluation/evaluation_k.py --path ./mlruns/5/5061122f729446f19833ac0db8a70d05/artifacts/gru_klearn_model
```

### `evaluation.py` can be called with the following models, if they exist.
- basic_tf_model
- bi_tf_model
- grutf_tf_model
- grutfre_tf_model
- fasttexttf_tf_model

### `evaluation_k.py` can be used with Ktrain models, if they exist.
- gru_klearn_model
- fasttext_klearn_model
- distilbert_klearn_model
- bigru_klearn_model


## Ktrain Models

Part of the models were learned with the help of [ktrain](https://github.com/amaiya/ktrain) by Arun S. Maiya. 
The paper for ktrain can be found here: https://arxiv.org/abs/2004.10703

> ktrain is a lightweight wrapper for the deep learning library TensorFlow Keras (and other libraries) to help build, train, and deploy neural networks and other machine learning models.

Ktrain was used for the models:
- gru_klearn
- fasttext_klearn
- distilbert_klearn
- bigru_klearn


## Parameters

```
usage: main.py [-h] [--trainer TRAINER] [--data DATA] [--epochs EPOCHS] [--learning_rate LEARNING_RATE]
               [--max_features MAX_FEATURES] [--embedding_dim EMBEDDING_DIM] [--batch_size BATCH_SIZE]
               [--seed SEED]

optional arguments:
  -h, --help            show this help message and exit
  --trainer TRAINER     Trainer: basic_tf, bi_tf, bigru_klearn, distilbert_klearn, gru_klearn, gru_tf,
                        grure_tf, fasttext_klearn, fasttext_tf (default: basic_tf)
  --data DATA           Choose one of the following preprocessed data methods if you use an empty string
                        e.g. --data "" then the current files in ./datasets/train.txt
                        ./datasets/validate.txt ./datasets/test.txt will be used "whitelist_blacklist"
                        "whitelist_blacklist_unbalanced" "whitelist_blacklist_cutted"
                        "whitelist_blacklist_and_dga" "whitelist_blacklist_and_dga_cutted" (default: )
  --epochs EPOCHS       number of epochs (default: 5)
  --learning_rate LEARNING_RATE
                        the learning rate (default: 0.001)
  --max_features MAX_FEATURES
                        number of tokens, used on every trainer except distilbert_klearn (default: 10000)
  --embedding_dim EMBEDDING_DIM
                        size of embeddings, used only for tensorflow trainers (default: 32)
  --batch_size BATCH_SIZE
                        size of batch (default: 32)
  --seed SEED           The seed to be used (default: 42)
  ```


## 3rd Party Licenses

The following Tools are used:

- [Python](https://docs.python.org/3/license.html)
- [ktrain](https://github.com/amaiya/ktrain/blob/master/LICENSE)
- [keras-tuner](https://github.com/keras-team/keras-tuner/blob/master/LICENSE)
- [TensorFlow](https://github.com/tensorflow/tensorflow/blob/master/LICENSE)
- [TensorFlow Models](https://github.com/tensorflow/models/blob/master/LICENSE)
- [TensorFlow Text](https://github.com/tensorflow/text/blob/master/LICENSE)
- [seaborn](https://github.com/mwaskom/seaborn/blob/master/LICENSE)
- [Matplotlib](https://matplotlib.org/stable/users/license.html)
- [MLflow](https://github.com/mlflow/mlflow/blob/master/LICENSE.txt)
- [sklearn](https://github.com/scikit-learn/scikit-learn/blob/main/COPYING)
- [pdoc](https://github.com/pdoc3/pdoc/blob/master/LICENSE.txt)  [only for developer documentation generation]
