#!/usr/bin/env python3
############################################################
# Advanced whitelist cleaning.
############################################################
# Optional:
# - ./datasets/data/dgas_preprocessed.csv
############################################################
if __name__ == '__main__':
    from os import path
    import sys
    sys.path.append(path.join(path.dirname(__file__), '..'))
############################################################

from warnings import filterwarnings

import pandas as pd
from pandas.core.frame import DataFrame

from tld import get_fld
from os.path import exists

def first_level(domain):
    """
    Get the first level domain (fld).
    Definition fld example: google.com

    Arguments
    ---------
    domain : str
        Domain name.

    Returns
    -------
    str
        First level domain of the given domain name.
    """
    return get_fld(domain, fix_protocol=True, fail_silently=True)


def has_valid_first_level_domain(domain):
    """
    Check if the domain has a valid first level domain.
    Definition fld example: google.com

    Arguments
    ---------
    domain : str
        Domain name.

    Returns
    -------
    bool
        True if domain has a valid first level domain; otherwise False.
    """
    return first_level(domain) is not None


def filter_with_regex(df, regex) -> DataFrame:
    """
    Filter data frame with regex pattern.

    Arguments
    ---------
    df : pf.DataFrame
        Data frame which should be filtered.
    regex : str
        Regex pattern.

    Returns
    -------
    pf.DataFrame
        Filtered data frame.
    """
    idx = df.str.contains(regex, regex=True, na=False)
    return df[~idx]


def main():
    """
    Main entry for the whitelist clean script.
    """
    whitelist_path = './datasets/data/whitelist.txt'
    whitelist_df = pd.read_csv(whitelist_path, header=None, names=['domain'])


    whitelist_df = whitelist_df[whitelist_df['domain'].apply(has_valid_first_level_domain)]

    dga_in_whitelist_path = "./datasets/data/dgas_preprocessed.csv"

    if exists(dga_in_whitelist_path):
        dgas_df = pd.read_csv(dga_in_whitelist_path, header=None, names=['domain'])
        whitelist_df = whitelist_df[~whitelist_df.isin(dgas_df)].dropna()

    whitelist_df = whitelist_df.reset_index()['domain']

    filterwarnings('ignore', 'This pattern has match groups')
    pattern_list = [
        r'^cache-.*fastly.*',
        r'^m\d{1,2}\..+\.',
        r'^v1\..+\.',
        r'^\d+',
        r'^(?:.*?\.){5,}',
        r'\b[0-9a-f]{8}\b-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-\b[0-9a-f]{12}\b',
        r'[^\.]{30,}',
    ]
    for pattern in pattern_list:
        whitelist_df = filter_with_regex(whitelist_df, pattern)

    print('Whitelist length:', len(whitelist_df))
    whitelist_df.to_csv(
        './datasets/data/whitelist_cleaned.txt',
        header=False,
        index=False,
    )


if __name__ == '__main__':
    main()
