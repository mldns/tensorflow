#!/usr/bin/env sh

mkdir -p "./datasets/data"
./scripts/download_and_prepare_white_and_blacklist.py --out-dir "./datasets/data"
./scripts/advanced_whitelist_cleaning.py
./scripts/prepare_split_datasets.py
cp ./datasets/data-prepared-cleaned/whitelist_blacklist/{test,train,validate}.txt ./datasets/
