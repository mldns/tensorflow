#!/usr/bin/env bash
# cspell:words bigru klearn grure cuda
# cspell:enableCompoundWords

trainers=(
    "basic_tf"
    "bi_tf"
    "bigru_klearn"
    "distilbert_klearn"
    "gru_klearn"
    "gru_tf"
    "grure_tf"
    "fasttext_klearn"
    "fasttext_tf"
)
data_paths=(
    "whitelist_blacklist"
    "whitelist_blacklist_unbalanced"
    "whitelist_blacklist_cutted"
    "whitelist_blacklist_and_dga"
    "whitelist_blacklist_and_dga_cutted"
)
seeds=(42 365 4711)

echo "trainers: ${trainers[*]}"
echo "data:     ${data_paths[*]}"
echo "seeds:    ${seeds[*]}"

trap "rm --force ./datasets/{test,train,validate}.txt" EXIT

for seed in ${seeds[*]}; do
    for data_path in ${data_paths[*]}; do
        echo "$data_path"

        if [ ! -d "./datasets/data-prepared-cleaned/$data_path" ]; then
            continue
        fi
        rm --force ./datasets/{test,train,validate}.txt
        cp ./datasets/data-prepared-cleaned/"$data_path"/{test,train,validate}.txt ./datasets/

        for trainer in ${trainers[*]}; do
            echo "$trainer"

            epochs=10
            re=".*bert.*"
            if [[ $trainer =~ $re ]]; then
                epochs=1
            fi

            python main.py --trainer "$trainer" --epochs "$epochs" --data "$data_path" --seed "$seed"
            sleep 5
        done
    done
done
