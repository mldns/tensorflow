#!/usr/bin/env python3
############################################################
# Prepare and split datasets.
############################################################
if __name__ == '__main__':
    from os import path
    import sys
    sys.path.append(path.join(path.dirname(__file__), '..'))
############################################################

import pandas as pd
from csv import QUOTE_NONE as CSV_QUOTE_NONE

from os import makedirs
from os.path import exists

from datasets.selection import DataSelection


def write_data_to_csv(X, y, csv_path):
    """
    Write data csv.

    Arguments
    ---------
    X : array, np.ndarray or ndarray-like
        List of domains.
    y : array, np.ndarray or ndarray-like
        List of labels (according to the list of domains).
    csv_path : str
        CSV file path, where the data should be saved.
    """
    df = pd.DataFrame(zip(y, X), columns=['label','domain'])
    df[['label','domain']].to_csv(
        csv_path,
        index=False,
        header=False,
        quoting=CSV_QUOTE_NONE,
    )
    

def main():
    methods = [
        "whitelist_blacklist",
        "whitelist_blacklist_unbalanced",
        "whitelist_blacklist_cutted",
        "whitelist_blacklist_and_dga",
        "whitelist_blacklist_and_dga_cutted",
    ]

    for method in methods:
        if "dga" in method and not exists("./datasets/data/dgas_preprocessed.csv"):
            continue
        print(method)

        dir = f"./datasets/data-prepared/{method}"
        makedirs(dir, exist_ok=True)

        fn = getattr(DataSelection, method)

        X_train, y_train, X_validate, y_validate, X_test, y_test = fn(
            allow_csv="./datasets/data/whitelist.txt",
            deny_csv="./datasets/data/blacklist.txt",
            dga_csv="./datasets/data/dgas_preprocessed.csv",
        )

        write_data_to_csv(X_train, y_train, f"{dir}/train.txt")
        write_data_to_csv(X_validate, y_validate, f"{dir}/validate.txt")
        write_data_to_csv(X_test, y_test, f"{dir}/test.txt")

        dir = f"./datasets/data-prepared-cleaned/{method}"
        makedirs(dir, exist_ok=True)

        fn = getattr(DataSelection, method)
        X_train, y_train, X_validate, y_validate, X_test, y_test = fn(
            allow_csv="./datasets/data/whitelist_cleaned.txt",
            deny_csv="./datasets/data/blacklist.txt",
            dga_csv="./datasets/data/dgas_preprocessed.csv",
        )

        write_data_to_csv(X_train, y_train, f"{dir}/train.txt")
        write_data_to_csv(X_validate, y_validate, f"{dir}/validate.txt")
        write_data_to_csv(X_test, y_test, f"{dir}/test.txt")


if __name__ == '__main__':
    main()
