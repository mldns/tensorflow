#!/usr/bin/env bash

rm -f scripts/evaluation.csv
evaluation_filenames=$(ls -1 eval/ | sed -e 's/^/eval\//' | grep -vP "bigru")
echo "name,accuracy,top_recall,bad_recall,top_precision,bad_precision" >> scripts/evaluation.csv
write_to_csv() {
    f=$1
    echo $f
    name=$(basename $f | cut -f 1 -d '.')
    accuracy=$(cat $f | grep accuracy | awk '{print $2}')
    top_recall=$(cat $f | grep -P "^\s+pos|^\s+1" | awk '{print $3}')
    bad_recall=$(cat $f | grep -P "^\s+neg|^\s+0" | awk '{print $3}')
    top_precision=$(cat $f | grep -P "^\s+pos|^\s+1" | awk '{print $2}')
    bad_precision=$(cat $f | grep -P "^\s+neg|^\s+0" | awk '{print $2}')

    echo "$name,$accuracy,$top_recall,$bad_recall,$top_precision, $bad_precision" >> scripts/evaluation.csv
}

for f in $evaluation_filenames; do
    write_to_csv $f
done
